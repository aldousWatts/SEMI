declare function CompileErrorReport(error: PlayFabModule.IPlayFabError): string;
/** The string representation of a number, or the number itself */
declare type NumberString = string | number;
declare type SkillID = number;
/** An index of items */
declare type ItemID = number;
declare type PetID = number;
declare type BankID = number;
declare type SpellID = number;
declare type AncientID = number;
declare type CurseID = number;
declare type AuroraID = number;
declare type AltmagicID = number;
declare type MonsterID = number;
declare type CharacterID = number;
declare type EquipSetID = 0 | 1 | 2 | 3;
declare type EquippedFoodID = 0 | 1 | 2 | 3;
declare type EquipSlotID = number;
declare type AttackStyleID = number;
declare type CombatAreaID = number;
declare type SlayerAreaID = number;
/** The type of a combat area. 0 for normal, 1 for slayer, 2 for dungeons */
declare type AreaType = 0 | 1 | 2;
declare type DungeonID = number;
declare type LootID = number;
declare type PrayerID = number;
/** An index of playerSpecialAttacks */
declare type PlayerSpecialID = number;
declare type EnemySpecialID = number;
declare type SlayerTier = number;
declare type SaveString = string;
declare type TimeoutID = number;
declare type BankTabID = number;
declare type ObjectKey = string | number | symbol;
declare type FarmingAreaID = number;
declare type FiremakingID = number;
/** An index of fishingItems or fishing mastery */
declare type FishingID = number;
/** An index of FishingAreas */
declare type FishingAreaID = number;
/** An index of FishingAreas[i].fish */
declare type FishID = number;
/** An index of fletchingItems */
declare type FletchingID = number;
declare type FletchLog = number;
declare type FletchingCategory = 0 | 1 | 2 | 3 | 4 | 5;
/** An index of herbloreItemData */
declare type HerbloreItemID = number;
declare type HerbloreCategory = 0 | 1 | 2;
declare type HerbloreTier = 0 | 1 | 2 | 3;
declare type AmmoType = 0 | 1 | 2 | 3;
declare type PageID = number;
declare type NotificationType = 'success' | 'info' | 'danger';
/** Index of glovesTracker, gloveID,glovesCost,glovesActions */
declare type GloveID = number;
/** Index of thievingNPC */
declare type ThievingID = number;
/** Index of tutorialTips */
declare type TutorialtipID = number;
/** Index of agilityObstacles */
declare type ObstacleID = number;
/** Index of passive pillars */
declare type PillarID = number;
/** Categories of agility obstacles */
declare type ObstacleCategories = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
/** Current gamemode, 0 is normal, 1 is hardcore, 2 is adventure, 3 is CHAOS */
declare type GameMode = 0 | 1 | 2 | 3;
/** Index of rockData,oreTypes */
declare type MiningID = number;
interface ReqCheck {
    reqID: number;
    bankID: number;
    check: number;
}
interface QtyReqCheck extends ReqCheck {
    qty: number;
    itemID?: ItemID;
}
/** Generic Interface for storing itemID quantity pairs */
interface ItemQuantity {
    id: ItemID;
    qty: number;
}
interface BankItem {
    id: ItemID;
    qty: number;
    tab: BankTabID;
    sellsFor: number;
    stackValue: number;
    locked: boolean;
}
interface BankDefaultItem {
    itemID: ItemID;
    tab: BankTabID;
}
interface BankSearch {
    id: ItemID;
    qty: number;
    name: string;
    category: string;
    description: string;
    type: string;
    tab: BankTabID;
}
interface PlayerCombatData {
    /** Current HP of player */
    hitpoints: number;
    /** Player is stunned and can't attack */
    stunned: boolean;
    /** Number of playerTimers left till stun wears off*/
    stunTurns: number;
    /** Base attack speed of player [ms] before attackSpeedDebuff/attackSpeedBuff */
    baseAttackSpeed: number;
    /** % decrease to baseAttackSpeed (multiplicative with other speed bonuses) */
    attackSpeedDebuff: number;
    /** Number of playerTimers left until slow wears off */
    attackSpeedDebuffTurns: number;
    /** @deprecated Unused Property */
    increasedDamageReduction: number;
    /** Player is sleeping and can't attack */
    sleep?: boolean;
    /** Number of playerTimers left till sleep wears off */
    sleepTurns?: number;
    /** Player has burn debuff active */
    isBurning?: boolean;
    /** Total % of player maxHitpoints that burn debuff takes */
    burnDebuff?: number;
    /** Player is under the effects of an enemy special stat debuff */
    activeDebuffs?: boolean;
    /** Number of enemyTimers left till attack debuff wears off */
    debuffTurns?: number;
    /** % decrease to player melee evasion from enemy spec (multiplicative with other decreases) */
    meleeEvasionDebuff?: number;
    /** % decrease to player ranged evasion from enemy spec (multiplicative with other decreases) */
    rangedEvasionDebuff?: number;
    /** % decrease to player magic evasion from enemy spec (multiplicative with other decreases) */
    magicEvasionDebuff?: number;
    /** % decrease to maximumAttackRoll from enemy spec (additive with other decreases) */
    decreasedAccuracy?: number;
    /** Number of affliction debuffs stacked on player */
    afflictedStacks?: number;
    /** Player has special attack */
    hasSpecialAttack?: boolean;
    /** Array of special attack IDs the player has */
    specialAttackID?: PlayerSpecialID[];
    /** Increased minimum hit from Charged Aurora */
    increasedMinHit?: number;
    /** Player has Mark of Death debuff active removePlayerMarkOfDeath, applyMarkOfDeathToPlayer are relevant*/
    markOfDeath?: boolean;
    /** Number of Mark of Death debuff stacks player has. */
    markOfDeathStacks?: number;
    /** @deprecated This seems to not actually do anything */
    markOfDeathTurns?: number;
    /** % Healing from damage from Fervor Aurora (additive with other increases) */
    lifesteal?: number;
    /** decrease to playerAttackSpeed in [ms] from Surge Aurora (applies after all other bonuses) */
    attackSpeedBuff?: number;
    /** % increase to maximumRangedDefenceRoll from Surge Aurora (multiplicative with other increases) */
    rangedEvasionBuff?: number;
    /** Flat increase to baseMaxHit from Fury Aurora (applies before bonuses from modifiers) */
    increasedMaxHit?: number;
    /** % increase to Magic Evasion from Fury Aurora (multiplicative with other increases) */
    magicEvasionBuff?: number;
    /** % increase to Melee Evasion from Fervor Aurora (multiplicative with other increases) */
    meleeEvasionBuff?: number;
    /** @deprecated Unused property, use playerModifiers instead */
    slayerAreaEffectNegationPercent?: number;
    /** @deprecated Unused property, use playerModifiers instead */
    slayerAreaEffectNegationFlat?: number;
    /**
     * Combat levels used to compute player stats
     * 0: Attack
     * 1: Strength
     * 2: Defence
     * 3: Hitpoints
     * 4: Ranged
     * 5: Magic
     * 6: Prayer
     */
    combatLevels?: number[];
    /** Stats from players Equipment */
    baseStats?: PlayerBaseStats;
    /**
     * Bonuses from combat potions
     * 0: Melee Accuracy
     * 1: Melee Evasion
     * 2: Melee Max Hit
     * 3: Ranged Evasion, Ranged Accuracy
     * 4: Ranged Max Hit
     * 5: Magic Evasion, Magic Accuracy
     * 6: Magic Max Hit
     * 7: Regeneration (but this value isn't used)
     * 8: Damage Reduction
     */
    herbloreBonus?: number[];
    /** Free skill levels used to compute maximumAttackRoll from combat style */
    attackStyleBonusAccuracy?: number;
    /** Free skill levels used to compute baseMaxHit from combat style */
    attackStyleBonusStrength?: number;
    maximumDefenceRoll?: number;
    maximumRangedDefenceRoll?: number;
    maximumMagicDefenceRoll?: number;
}
declare type PlayerBaseStats = {
    /** Melee Attack Bonus [stab,slash,defend] */
    attackBonus: [number, number, number];
    /** Melee Defence Bonus */
    defenceBonus: number;
    /** Melee Strength Bonus */
    strengthBonus: number;
    /** % Damage Reduction */
    damageReduction: number;
    /** Ranged Attack Bonus */
    attackBonusRanged: number;
    /** Ranged Defence Bonus */
    defenceBonusRanged: number;
    /** Ranged Strength Bonus */
    strengthBonusRanged: number;
    /** Magic Attack Bonus */
    attackBonusMagic: number;
    /** Magic Defence Bonus */
    defenceBonusMagic: number;
    /** % Bonus to Magic Max Hit */
    damageBonusMagic: number;
};
interface OldEquipmentSet {
    equipment: number[];
    ammo: number;
    summonAmmo: [number, number];
}
interface ItemQuantity2 {
    itemID: ItemID;
    qty: number;
}
interface MonsterStat {
    monsterID: MonsterID;
    stats: number[];
}
interface CookingItem {
    itemID: ItemID;
    cookingID: number;
    cookingLevel: number;
}
interface LogsForFire {
    itemID: ItemID;
    firemakingID: number;
}
interface CraftingItem {
    itemID: ItemID;
    craftingLevel: number;
    craftingID: number;
}
interface Seed {
    itemID: ItemID;
    level: number;
}
interface FarmingPatch {
    type: string;
    seedID: ItemID;
    compost: number;
    timePlanted: number;
    setInterval: number;
    timeout: null | TimeoutID;
    hasGrown: boolean;
    unlocked: boolean;
    gloop: boolean;
    level: number;
    cost: number;
}
interface FarmingArea {
    id: number;
    areaName: string;
    patches: FarmingPatch[];
}
interface FishingItem {
    itemID: ItemID;
    fishingLevel: number;
    fishingID: FishingID;
}
declare type Loot = [ItemID, number];
declare type LootTable = Loot[];
interface FletchingItem {
    itemID: ItemID;
    fletchingLevel: number;
    fletchingID: FletchingID;
    fletchingCategory: FletchingCategory;
}
interface RaidHistory {
    skillLevels: number[];
    equipment: number[];
    ammo: number;
    inventory: ItemQuantity[];
    food: ItemQuantity2;
    wave: number;
    kills: number;
    timestamp: number;
    raidCoinsEarned: number;
}
interface HerbloreItem {
    id: HerbloreItemID;
    name: string;
    itemID: ItemID[];
    category: HerbloreCategory;
    herbloreLevel: number;
    herbloreXP: number;
    consumesOn?: PotionConsumption;
}
interface HerbloreBonus {
    itemID: ItemID;
    bonus: [null | PotionBonusID, null | number];
    charges: number;
}
declare type HerbloreBonuses = NumberDictionary<HerbloreBonus>;
declare type PotionBonusID = number;
interface ItemStat {
    /**
     * 0: timesFound
     * 1: timesSold
     * 2: gpFromSale
     * 3: deathCount
     * 4: damageTaken
     * 5: damageDealt
     * 6: missedAttacks
     * 7: timesEaten
     * 8: healedFor
     * 9: totalAttacks
     * 10: amountUsedInCombat
     * 11: timeWaited
     * 12: timesDied
     * 13: timesGrown
     * 14: harvestAmount
     * 15: enemiesKilled
     * 16: timesOpened
     */
    stats: number[];
    itemID: ItemID;
}
interface OfflineBase {
    skill: SkillID;
    timestamp: number;
}
/** Used for woodcutting */
interface OfflineWoodcut extends OfflineBase {
    action: number[];
}
/** Used for woodcutting */
interface OfflineMagic extends OfflineBase {
    action: [number, [number, number, number]];
}
interface OfflineUnset {
    skill: null;
    action: null;
    timestamp: null;
}
/** Used for all other skills */
interface OfflineSkill extends OfflineBase {
    action: number;
}
/** Used for fishing, fletching, alt magic */
interface OfflineTuple extends OfflineBase {
    action: [number, number];
}
declare type Offline = OfflineWoodcut | OfflineTuple | OfflineUnset | OfflineSkill | OfflineMagic;
declare type SumFunction = (arr: number[]) => number;
declare type OldMasteryArray = {
    mastery: number;
    masteryXP: number;
}[];
declare type MasteryCheckPoint = number;
declare type Mastery = NumberDictionary<MasteryData>;
interface MasteryData {
    pool: number;
    xp: number[];
}
interface Pet {
    name: string;
    description: string;
    media: string;
    acquiredBy: string;
    skill: SkillID;
    chance?: number;
    killCount?: number;
    ignoreCompletion?: boolean;
    modifiers?: ModifierData;
    obtained?: {
        dungeonCompletion: [DungeonID, number][];
    };
    activeInRaid: boolean;
}
interface RockData {
    maxHP: number;
    damage: number;
    depleted: boolean;
    respawnTimer: null | TimeoutID;
}
/** Mastery ID for runecrafting */
declare type RunecraftingID = number;
declare type RunecraftingCategory = 0 | 1 | 2 | 3 | 4 | 5 | 6;
interface RunecraftingItem {
    itemID: ItemID;
    runecraftingLevel: number;
    runecraftingID: RunecraftingID;
    runecraftingCategory: RunecraftingCategory;
}
interface SaveGame {
    firstTime: typeof firstTime;
    username: typeof username;
    nameChanges: typeof nameChanges;
    gp: typeof gp;
    currentBankUpgrade: number;
    skillXP: typeof skillXP;
    /** Redundant, can be reproduced on load */
    skillLevel: typeof skillLevel;
    /** Redundant, can be reproduced on load */
    nextLevelProgress: typeof nextLevelProgress;
    treeMasteryData: OldMasteryArray;
    currentAxe: number;
    treeCutLimit: number;
    /** Contains redundant data, can be reduced in size */
    bank: typeof bank;
    bankMax: typeof bankMax;
    ignoreBankFull: typeof ignoreBankFull;
    /** Contains redundant data, can be reduced in size */
    statsGeneral: typeof statsGeneral;
    /** Contains redundant data, can be reduced in size */
    statsWoodcutting: typeof statsWoodcutting;
    logsMastery: OldMasteryArray;
    /** Contains redundant data, can be reduced in size */
    statsFiremaking: typeof statsFiremaking;
    fishMastery: OldMasteryArray;
    currentRod: number;
    /** Contains redundant data, can be reduced in size */
    statsFishing: typeof statsFishing;
    cookingMastery: OldMasteryArray;
    upgradedToRange: boolean;
    /** Contains redundant data, can be reduced in size */
    statsCooking: typeof statsCooking;
    defaultPageOnLoad: typeof defaultPageOnLoad;
    miningOreMastery: OldMasteryArray;
    /** Contains redundant data, can be reduced in size */
    statsMining: typeof statsMining;
    currentPickaxe: number;
    /** Contains redundant data, can be reduced in size */
    statsSmithing: typeof statsSmithing;
    levelUpScreen: typeof levelUpScreen;
    gameUpdateNotification: typeof gameUpdateNotification;
    /** Redundant, can be reproduced on load */
    equippedItems: number[];
    attackStyle: number;
    /** Contains redundant data, can be reduced in size */
    combatData: MinCombatData;
    currentCombatFood: EquippedFoodID;
    /** Can be reduced in size by changing to array */
    equippedFood: {
        itemID: number;
        qty: number;
    }[];
    smithingMastery: OldMasteryArray;
    /** Contains redundant data, can be reduced in size */
    statsCombat: typeof statsCombat;
    continueThievingOnStun: typeof continueThievingOnStun;
    thievingMastery: OldMasteryArray;
    farmingMastery: OldMasteryArray;
    showItemNotify: typeof showItemNotify;
    /** Contains redundant data, can be reduced in size */
    glovesTracker: typeof glovesTracker;
    currentCookingFire: number;
    /** Contains redundant data, can be reduced in size */
    rockData: typeof rockData;
    fletchingMastery: OldMasteryArray;
    craftingMastery: OldMasteryArray;
    /** Redundant, can be reproduced on load*/
    ammo: number;
    myBankVersion: typeof myBankVersion;
    /** Contains redundant data, can be reduced in size */
    statsThieving: typeof statsThieving;
    /** Contains redundant data, can be reduced in size */
    statsFarming: typeof statsFarming;
    /** Contains redundant data, can be reduced in size */
    statsFletching: typeof statsFletching;
    /** Contains redundant data, can be reduced in size */
    statsCrafting: typeof statsCrafting;
    autoRestartDungeon: typeof autoRestartDungeon;
    autoSaveCloud: typeof autoSaveCloud;
    selectedSpell: number;
    runecraftingMastery: OldMasteryArray;
    darkMode: typeof darkMode;
    buyQty: typeof buyQty;
    itemLog: typeof itemLog;
    dungeonCompleteCount: typeof dungeonCompleteCount;
    sellQty: typeof sellQty;
    /** Contains redundant data, can be reduced in size */
    statsRunecrafting: typeof statsRunecrafting;
    showGPNotify: typeof showGPNotify;
    enableAccessibility: typeof enableAccessibility;
    /** Old save variable that can be deleted */
    showSkillNav: boolean;
    accountGameVersion: typeof accountGameVersion;
    prayerPoints: number;
    slayerCoins: number;
    /** Can be serialized to reduce size */
    slayerTask: OldSlayerTask[];
    showEnemySkillLevels: typeof showEnemySkillLevels;
    monsterStats: typeof monsterStats;
    itemStats: typeof itemStats;
    confirmationOnClose: typeof confirmationOnClose;
    herbloreMastery: OldMasteryArray;
    /** Contains redundant data, can be reduced in size */
    newFarmingAreas: typeof newFarmingAreas;
    equipmentSets: OldMasteryArray;
    selectedEquipmentSet: EquipSetID;
    currentAutoEat: number;
    equipmentSetsPurchased: boolean[];
    /** Contains redundant data, can be reduced in size */
    herbloreBonuses: typeof herbloreBonuses;
    autoPotion: typeof autoPotion;
    autoUseSpecialAttack: typeof autoUseSpecialAttack;
    showHPNotify: typeof showHPNotify;
    /** Contains redundant data, can be reduced in size */
    statsHerblore: typeof statsHerblore;
    offline: typeof offline;
    selectedAttackStyle: number[];
    showCommas: typeof showCommas;
    showVirtualLevels: typeof showVirtualLevels;
    formatNumberSetting: typeof formatNumberSetting;
    /** Can be converted to array to reduce size */
    tutorialTips: typeof tutorialTips;
    saveTimestamp: typeof saveTimestamp;
    secretAreaUnlocked: typeof secretAreaUnlocked;
    equipmentSwapPurchased: boolean;
    godUpgrade: boolean[];
    lockedItems: typeof lockedItems;
    showSaleNotifications: typeof showSaleNotifications;
    showShopNotifications: typeof showShopNotifications;
    pauseOfflineActions: typeof pauseOfflineActions;
    showCombatMinibar: typeof showCombatMinibar;
    showCombatMinibarCombat: typeof showCombatMinibarCombat;
    activeAurora: number | null;
    currentGamemode: typeof currentGamemode;
    petUnlocked: typeof petUnlocked;
    showSkillMinibar: typeof showSkillMinibar;
    saveStateBeforeRaid: [] | [number, EquipSetID, EquippedFoodID];
    golbinRaidHistory: typeof golbinRaidHistory;
    golbinRaidStats: typeof golbinRaidStats;
    raidCoins: typeof raidCoins;
    disableAds: typeof disableAds;
    /** Can be serialized to reduce size */
    SETTINGS: typeof SETTINGS;
    /** Can be serialized to reduce size */
    MASTERY: typeof MASTERY;
    useCombinationRunes: typeof useCombinationRunes;
    firstTimeLoad: typeof firstTimeLoad;
    slayerTaskCompletion: number[];
    autoSlayerUnlocked: boolean;
    autoSlayer: typeof autoSlayer;
    /** Redundant, can be reproduced on load */
    itemsAlreadyFound: typeof itemsAlreadyFound;
    xmasPresents: number;
    /** Can be massively reduced in size */
    shopItemsPurchased: typeof shopItemsPurchased;
    titleNewsID: typeof titleNewsID;
    chosenAgilityObstacles: typeof chosenAgilityObstacles;
    skillsUnlocked: typeof skillsUnlocked;
    agilityObstacleBuildCount: typeof agilityObstacleBuildCount;
    agilityPassivePillarActive: typeof agilityPassivePillarActive;
    scheduledPushNotifications: typeof scheduledPushNotifications;
    randomModifiers: {
        equipment: NumberDictionary<Partial<ModifierObject<SkillModifierData[], number>>>;
        player: NumberDictionary<Partial<ModifierObject<SkillModifierData[], number>>>;
    };
    summoningData: typeof summoningData;
}
declare type SaveKey = keyof SaveGame;
interface SmithingItem {
    itemID: ItemID;
    smithingLevel: number;
    smithingID: number;
    name: string;
    category: SmithingCategory;
}
/** Index of smithingItems */
declare type SmithingID = number;
declare type SmithingCategory = number;
interface GameStat {
    stat: string;
    id: string;
    count: number;
}
/** Index of trees */
declare type WoodcuttingID = number;
interface BankSearchOpts {
    shouldSort: true;
    tokenize: true;
    matchAllTokens: true;
    findAllMatches: true;
    threshold: 0.1;
    location: 0;
    distance: 100;
    maxPatternLength: 32;
    minMatchCharLength: 1;
    keys: ['name', 'category', 'id', 'type', 'description'];
}
/** Generic base for all item like arrays */
interface GenericItem extends BaseItem {
    /** Item has an animated image */
    hasAnimation?: boolean;
    /** Link to animate image source */
    mediaAnimation?: string;
    /** Item can be upgraded */
    canUpgrade?: boolean;
    /** Item to upgrade to */
    trimmedItemID?: number;
    /** Items required to upgrade */
    itemsRequired?: number[][];
    /** GP cost to upgrades */
    trimmedGPCost?: number;
    /** Multiple of the item can be upgraded at once */
    canMultiUpgrade?: boolean;
    /** Item can be opened (e.g. is a chest) */
    canOpen?: boolean;
    /** ItemIDs and weights of drops */
    dropTable?: LootTable;
    /** Quantities of drops */
    dropQty?: number[];
    /** Used to create SHOP variable, but otherwise do not use */
    buysFor?: number;
    /** Unused flag for bones, game uses prayerPoints prop instead */
    isBones?: boolean;
    /** Designates item as bone. Value is base pp given on burying */
    prayerPoints?: number;
    /** Designates item as food */
    canEat?: boolean;
    /** value*numberMultiplier is base hp of food. */
    healsFor?: number;
    /** Item can be read from bank */
    canRead?: boolean;
    /** When read, sets title of Swal */
    readTitle?: string;
    /** When read, sets html of Swal */
    readText?: string;
    /** @deprecated ID for birthday event cluescrolls. No longer in use. */
    clueID?: 0 | 1 | 2 | 3;
    /** Item can be claimed as token in bank */
    isToken?: boolean;
    /** Flags token as mastery token, and is the Skill to give masteryxp to */
    skill?: SkillID;
    /** Flags token as bank token */
    isBankToken?: boolean;
    /** [SkillID,MasteryID] of producing the item*/
    masteryID?: [SkillID, number];
    /** MasteryID for item for Firemaking */
    firemakingID?: number;
    /** Identifies item as able to be cooked. Also MasteryID for cooking */
    cookingID?: number;
    /** Cooking level required to cook item */
    cookingLevel?: number;
    /** Cooking xp for cooking item */
    cookingXP?: number;
    /** ItemID to give when item cooked */
    cookedItemID?: ItemID;
    /** ItemID to give when item burned */
    burntItemID?: ItemID;
    /** Identifies item as fish, should also be identical to MasteryID for it */
    fishingID?: number;
    /** Fishing level required to catch item */
    fishingLevel?: number;
    /** Fishing XP for catching item */
    fishingXP?: number;
    /** Min interval [ms] to catch item */
    minFishingInterval?: number;
    /** Max interval [ms] to catch item */
    maxFishingInterval?: number;
    /** If present fish gives strength xp when caught */
    strengthXP?: number;
    /** Identifies item as specialItems and indicates weight in loot-table */
    fishingCatchWeight?: number;
    /** Experience earned for Mining item */
    miningXP?: number;
    /** Smithing level required to make item */
    smithingLevel?: number;
    /** Smithing xp for making item */
    smithingXP?: number;
    /** Items required to smith item */
    smithReq?: ItemQuantity[];
    /** For seeds, identifies crop type. For smithing identifies metal. */
    tier?: string;
    /** If present, sets smithed quantity to value, else it is 1 */
    smithingQty?: number;
    /** Farming level required to grow seed */
    farmingLevel?: number;
    /** Farming XP given when planting, and xp per harvest qty when harvesting */
    farmingXP?: number;
    /** Quantity of seeds required to plant */
    seedsRequired?: number;
    /** Time to grow seed in [s] */
    timeToGrow?: number;
    /** ItemID given when harvesting seed */
    grownItemID?: ItemID;
    /** Items required to fletch item */
    fletchReq?: ItemQuantity[];
    /** Base quantity given when fletched */
    fletchQty?: number;
    /** Fletching level required to fletch */
    fletchingLevel?: number;
    /** Base Fletching xp given when fletched */
    fletchingXP?: number;
    /** Tab of fletching page:
     * 0: Arrows
     * 1: Shortbows
     * 2: Longbows
     * 3: Gem-Tipped Bolts
     * 4: Crossbows
     * 5: Javelins
     */
    fletchingCategory?: FletchingCategory;
    /** Items required to craft */
    craftReq?: ItemQuantity[];
    /** Base quantity made when crafting */
    craftQty?: number;
    /** Crafting level required to make */
    craftingLevel?: number;
    /** Base Crafting XP per craft  */
    craftingXP?: number;
    /** Runecrafting level required to make */
    runecraftingLevel?: number;
    /** Base Runecrafting xp per runecraft */
    runecraftingXP?: number;
    /** Items required to runecraft */
    runecraftReq?: ItemQuantity[];
    /** Base quantity made when runecrafting */
    runecraftQty?: number;
    /** Runecrafting MasteryID of item */
    runecraftingID?: number;
    /** Tab of Runecrafting page:
     * 0: Standard Runes
     * 1: Combination Runes
     * 2: Staves & Wands
     * 3: Air Magic Gear
     * 4: Water Magic Gear
     * 5: Earth Magic Gear
     * 6: Fire Magic Gear
     */
    runecraftingCategory?: RunecraftingCategory;
    /** Item has stats that are viewable from Runecrafting page */
    hasStats?: boolean;
    /** Items required to brew potion */
    herbloreReq?: ItemQuantity[];
    /** Flags item as potion */
    isPotion?: boolean;
    /** Skill that potion applies to, used for herblore display */
    potionSkill?: SkillID;
    /** Value of potion's effect */
    potionBonus?: number;
    /** ID used to distinguish the effects of combat potions */
    potionBonusID?: PotionBonusID;
    /** Base charges of potion */
    potionCharges?: number;
    /** Page potion can be used on */
    potionPage?: PageID;
    /** Tier of potion */
    potionTier?: HerbloreTier;
    /** Unused flag for ranged weapons */
    isRanged?: boolean;
    /** Flags item as skill glove, and identifies index in data arrays */
    gloveID?: GloveID;
    /** Ammunition type:
     * 0: Arrows
     * 1: Bolts
     * 2: Javelins
     * 3: Throwing Knives
     */
    ammoType?: AmmoType;
    /** Ammunition type required to use ranged weapon:
     * 0: Arrows
     * 1: Bolts
     * 2: Javelins
     * 3: Throwing Knives
     */
    ammoTypeRequired?: AmmoType;
    /** Item has special attack */
    hasSpecialAttack?: boolean;
    /** Index of playerSpecialAttacks that weapon has */
    specialAttacks?: Attack[];
    /** Item provides free runes for spells */
    providesRune?: ItemID[];
    /** Quantity of runes set by providesRune */
    providesRuneQty?: number;
    /** Drop rate of Mysterious Stone */
    dropRate?: number;
    /** Melee strength bonus multiplier of Big Ol Ron */
    bossStrengthMultiplier?: number;
    /** Ranged strength bonus multiplier of Slayer Crossbow */
    slayerStrengthMultiplier?: number;
    /** Base drop rate of Crown of Rhaelyx components */
    baseDropRate?: number;
    /** Max drop rate of Crown of Rhaelyx components */
    maxDropRate?: number;
    /** increasedFarmingYield for Weird Gloop */
    harvestBonus?: number;
    /** Modifiers provided when item is equipped. Also contains modifiers for potions that are WIP */
    modifiers?: ModifierData;
    /** Modifiers applied to the enemy when item is equipped */
    enemyModifiers?: CombatModifierData;
    smithingBar?: ItemID;
    equipRequirements?: Requirement[];
    equipmentStats?: EquipStatPair[];
    summoningDescription?: string;
    summoningID?: number;
    summoningReq?: ItemQuantity[][];
    summoningTier?: number;
    summoningQty?: number;
    summoningLevel?: number;
    summoningXP?: number;
    summoningSkills?: SkillID[];
    validSlots?: SlotTypes[];
    occupiesSlots?: SlotTypes[];
    /** Determines the attacktype the player uses when equipped in weapon slot */
    attackType?: AttackType;
}
interface FindEnemyAreaFcn {
    (enemy: MonsterID, name?: true): string;
    (enemy: MonsterID, name: false): [number, number];
}
/** Item with universal Properties */
interface BaseItem {
    /** Categorization tag */
    category: string;
    /** Second Categorization tag */
    type: string;
    /** Display name of item. May contain html portions that must be replaced/filtered*/
    name: string;
    /** Base sale price of item */
    sellsFor: number;
    /** Local path to item image */
    media: string;
    /** ID of item */
    id: ItemID;
    /** Optional description of item */
    description?: string;
    /** Optional flag that indicates item should not count for Item Completion % */
    ignoreCompletion?: boolean;
    isEquipment: boolean;
}
declare type Shop = typeof SHOP;
interface Array<T> {
    sum: (prop: keyof T) => number;
}
declare type AgilityPillar = {
    name: string;
    description: string;
    cost: ObstacleCost;
    modifiers: ModifierData;
};
interface AgilityObstacle extends AgilityPillar {
    media: string;
    category: ObstacleCategories;
    interval: number;
    requirements: UnlockRequirement;
    completionBonuses: {
        stamina: number;
        xp: number;
        gp: number;
        slayerCoins: number;
        items: number[][];
    };
}
declare type ObstacleCost = {
    gp: number;
    slayerCoins: number;
    items: number[][];
};
declare type BankCache = NumberDictionary<number>;
declare type MasteryCache = NumberDictionary<NumberDictionary<number>>;
declare type MasteryLevelCache = NumberDictionary<{
    levels: number[];
}>;
interface PlayFabEventBody {
    [key: string]: any;
}
interface StringDictionary<T> {
    [index: string]: T;
}
interface NumberDictionary<T> {
    [index: number]: T;
}
declare type ShopCategory =
    | 'General'
    | 'SkillUpgrades'
    | 'Slayer'
    | 'Gloves'
    | 'Skillcapes'
    | 'Materials'
    | 'GolbinRaid';
declare type ShopCostTypes = 'gp' | 'slayerCoins' | 'items' | 'raidCoins';
declare type ShopCategoryData = {
    name: string;
    description: string;
    media: string;
    contains: {
        modifiers?: ModifierData;
        items?: [ItemID, number][];
        pet?: PetID;
    };
    cost: {
        gp: number;
        slayerCoins: number;
        items: [ItemID, number][];
        raidCoins?: number;
    };
    hasQty?: boolean;
    charges?: number;
    unlockRequirements: UnlockRequirement;
    buyLimit: [number, number, number, number];
    showBuyLimit?: boolean;
};
declare type UnlockRequirement = {
    customDescription?: string;
    shopItemPurchased?: [ShopCategory, number][];
    skillLevel?: [SkillID, number][];
    slayerTaskCompletion?: [SlayerTier, number][];
    dungeonCompletion?: [DungeonID, number][];
    completionPercentage?: number;
};
declare type SkillData = {
    /** Display name of skill */
    name: string;
    /** Image URL of skill icon */
    media: string;
    /** Skill has mastery levels */
    hasMastery: boolean;
    /** Unused: Maximum level of skill */
    maxLevel: number;
    masteryTokenID?: ItemID;
};
declare type MasteryMedia = {
    /** Image URL of mastery item */
    media: string;
};
declare type MasteryUnlock = {
    /** Mastery level for unlock */
    level: number;
    /** Description of unlock */
    unlock: string;
};
declare type MasteryPoolBonus = {
    /** Description of pool bonus */
    bonuses: string[];
};
interface SkillObject<T> {
    Woodcutting: T;
    Fishing: T;
    Firemaking: T;
    Cooking: T;
    Mining: T;
    Smithing: T;
    Attack: T;
    Strength: T;
    Defence: T;
    Hitpoints: T;
    Thieving: T;
    Farming: T;
    Ranged: T;
    Fletching: T;
    Crafting: T;
    Runecrafting: T;
    Magic: T;
    Prayer: T;
    Slayer: T;
    Herblore: T;
    Agility: T;
    Summoning: T;
}
declare type SkillName = keyof SkillObject<any>;
declare type Milestone = {
    /** Unlock level of milestone */
    level: number;
    /** Display name of milestone */
    name: string;
    /** URL of milestone image */
    media: string;
    /** @deprecated Unused property */
    alwaysShow?: boolean;
};
declare type GamemodeData = {
    name: string;
    media: string;
    description: string;
    rules: string[];
    textClass: string;
    btnClass: string;
    isPermaDeath: boolean;
    isEvent: boolean;
    endDate: number;
    combatTriangle: number;
    numberMultiplier: number;
    hasRegen: boolean;
};
declare type RandomModifier = {
    modifier: ModifierKeys;
    value: number | number[][];
};
declare type SummoningData = {
    Settings: SummoningSettings;
    Marks: SummoningMarks;
    Synergies: SummoningSynergies;
};
declare type SummoningSettings = {
    recipeGPCost: number;
};
declare type SummoningMarks = {
    Levels: number[];
};
declare type SummoningSynergies = NumberDictionary<NumberDictionary<SynergyData>>;
declare type SynergyData = {
    description: string;
    modifiers: ModifierData;
    enemyModifiers?: CombatModifierData;
    /** Presumably this is a typo */
    summoningSynergy_9_19?: number;
};
declare type PlayerSummoningData = {
    MarksDiscovered?: NumberDictionary<number>;
    defaultRecipe?: number[];
};
declare type SummoningItem = {
    itemID: ItemID;
    summoningLevel: number;
    summoningID: number;
    summoningCategory: number;
};
declare type SummoningSearch = {
    description: string;
    name1: string;
    name2: string;
    name1long: string;
    name2long: string;
    summon1: number;
    summon2: string;
};
declare type CombatLevels = {
    Hitpoints: number;
    Attack: number;
    Strength: number;
    Defence: number;
    Ranged: number;
    Magic: number;
    Prayer: number;
};
declare type MapToElement<Type> = {
    [Property in keyof Type]: HTMLElement;
};
declare type SlotTypes =
    | 'Helmet'
    | 'Platebody'
    | 'Platelegs'
    | 'Boots'
    | 'Weapon'
    | 'Shield'
    | 'Amulet'
    | 'Ring'
    | 'Gloves'
    | 'Quiver'
    | 'Cape'
    | 'Passive'
    | 'Summon1'
    | 'Summon2';
declare type EquipStatKey =
    | 'attackSpeed'
    | 'stabAttackBonus'
    | 'slashAttackBonus'
    | 'blockAttackBonus'
    | 'rangedAttackBonus'
    | 'magicAttackBonus'
    | 'meleeStrengthBonus'
    | 'rangedStrengthBonus'
    | 'magicDamageBonus'
    | 'meleeDefenceBonus'
    | 'rangedDefenceBonus'
    | 'magicDefenceBonus'
    | 'damageReduction'
    | 'summoningMaxhit';
declare type EquipStatPair = {
    key: EquipStatKey;
    value: number;
};
declare type TippyTooltip = import('tippy.js').Instance<import('tippy.js').Props>;
declare type TooltipInstances = {
    bank: TippyTooltip[];
    spellbook: TippyTooltip[];
    minibar: TippyTooltip[];
    combatInfo: TippyTooltip[];
    specialAttack: TippyTooltip[];
    equipmentSets: TippyTooltip[];
    masteryModal: TippyTooltip[];
    combatXP: TippyTooltip[];
    autoEat: TippyTooltip[];
    selectItemForMagic: TippyTooltip[];
    fletching?: TippyTooltip[];
    herblore?: TippyTooltip[];
    smithing?: TippyTooltip[];
    enemyAttackType?: TippyTooltip[];
    loot?: TippyTooltip[];
    crafting?: TippyTooltip[];
    prayerMenu?: TippyTooltip[];
    craftingRecipe?: TippyTooltip[];
    herbloreRecipe?: TippyTooltip[];
    selectMagic?: TippyTooltip[];
    runecrafting?: TippyTooltip[];
    runecraftingRecipe?: TippyTooltip[];
    fletchingRecipe?: TippyTooltip[];
    smithingRecipe?: TippyTooltip[];
    itemLog?: TippyTooltip[];
    monsterLog?: TippyTooltip[];
    petLog?: TippyTooltip[];
    agilityItemCosts?: TippyTooltip[];
    summoningSynergy?: TippyTooltip[];
    summoningRecipe?: TippyTooltip[];
    summoning?: TippyTooltip[];
};
declare type ShopPurchase = {
    category: ShopCategory;
    id: number;
    quantity: number;
};
declare type SweetAlertOptions = import('sweetalert2').SweetAlertOptions<*>;
interface FindEnemyAreaFcn {
    (enemy: MonsterID, name: true): string;
    (enemy: MonsterID, name: false): [number, number];
}
declare const enum SettingID {
    IgnoreBankFull = 1,
    DefaultPageOnLoad = 2,
    LevelUpScreen = 3,
    ContinueThievingOnStun = 4,
    ShowItemNotify = 5,
    AutoRestartDungeon = 6,
    AutoSaveCloud = 7,
    DarkMode = 8,
    ShowGPNotify = 9,
    ShowEnemySkillLevels = 12,
    ConfirmationOnClose = 13,
    EnableAccessibility = 14,
    AutoPotion = 15,
    AutoUseSpecialAttack = 16,
    ShowHPNotify = 17,
    AutoSlayerTask = 18,
    ShowCommas = 19,
    ShowVirtualLevels = 20,
    FormatNumberSetting = 21,
    PauseOfflineActions = 22,
    ShowSaleNotifications = 23,
    ShowShopNotifications = 24,
    ShowCombatMinibar = 25,
    ShowCombatMinibarCombat = 26,
    UseCombinationRunes = 27,
    ShowSkillMinibar = 28,
    DisableAds = 29,
    CurrentEquipDefault = 30,
    HideMaxLevel = 31,
    AutoSlayer = 32,
    ConfirmationCheckpoint = 33,
}
declare type NumBool = 0 | 1;
interface ChangeSettingsFcn {
    (s: SettingID.IgnoreBankFull, t: boolean, i?: boolean): void;
    (s: SettingID.DefaultPageOnLoad, t: PageID, i?: boolean): void;
    (s: SettingID.LevelUpScreen, t: NumBool, i?: boolean): void;
    (s: SettingID.ContinueThievingOnStun, t: boolean, i?: boolean): void;
    (s: SettingID.ShowItemNotify, t: NumBool, i?: boolean): void;
    (s: SettingID.AutoRestartDungeon, t: boolean, i?: boolean): void;
    (s: SettingID.AutoSaveCloud, t: boolean, i?: boolean): void;
    (s: SettingID.DarkMode, t: boolean, i?: boolean): void;
    (s: SettingID.ShowGPNotify, t: boolean, i?: boolean): void;
    (s: SettingID.ShowEnemySkillLevels, t: boolean, i?: boolean): void;
    (s: SettingID.ConfirmationOnClose, t: boolean, i?: boolean): void;
    (s: SettingID.EnableAccessibility, t: boolean, i?: boolean): void;
    (s: SettingID.AutoPotion, t: boolean, i?: boolean): void;
    (s: SettingID.ShowHPNotify, t: boolean, i?: boolean): void;
    (s: SettingID.AutoSlayerTask, t: true, i?: boolean): void;
    (s: SettingID.ShowCommas, t: boolean, i?: boolean): void;
    (s: SettingID.ShowVirtualLevels, t: boolean, i?: boolean): void;
    (s: SettingID.FormatNumberSetting, t: NumBool, i?: boolean): void;
    (s: SettingID.PauseOfflineActions, t: boolean, i?: boolean): void;
    (s: SettingID.ShowSaleNotifications, t: boolean, i?: boolean): void;
    (s: SettingID.ShowShopNotifications, t: boolean, i?: boolean): void;
    (s: SettingID.ShowCombatMinibar, t: boolean, i?: boolean): void;
    (s: SettingID.ShowCombatMinibarCombat, t: boolean, i?: boolean): void;
    (s: SettingID.ShowSkillMinibar, t: boolean, i?: boolean): void;
    (s: SettingID.DisableAds, t: boolean, i?: boolean): void;
    (s: SettingID.CurrentEquipDefault, t: boolean, i?: boolean): void;
    (s: SettingID.HideMaxLevel, t: boolean, i?: boolean): void;
    (s: SettingID.ConfirmationCheckpoint, t: boolean, i?: boolean): void;
}
declare type ValFcnForInput = (() => string) & ((value: string) => void);
interface JQueryStatic {
    (selector: '#username-set-main'): JQueryInputElement;
    (selector: '#username-set'): JQueryInputElement;
    (selector: '#searchTextbox'): JQueryInputElement;
    (selector: '#dropdown-content-custom-amount'): JQueryInputElement;
    (selector: '#dropdown-content-custom-amount-1'): JQueryInputElement;
    (selector: '#import-save-character-selection'): JQueryInputElement;
}
interface JQueryInputElement extends Omit<JQuery<HTMLElement>, 'val'> {
    val: ValFcnForInput;
}
interface Document {
    getElementById(elementID: 'username-change'): HTMLInputElement;
    getElementById(elementID: 'game-broke-error-msg'): HTMLTextAreaElement;
    getElementById(elementID: 'exportSaveField'): HTMLTextAreaElement;
    getElementById(elementID: 'exportSaveField2'): HTMLTextAreaElement;
    getElementById(elementID: 'exportSaveFieldUpdate'): HTMLTextAreaElement;
    getElementById(elementID: 'exportSaveFieldUpdate2'): HTMLTextAreaElement;
    getElementById(elementID: 'importSaveField'): HTMLTextAreaElement;
}
interface ObjectConstructor {
    keys(obj: Partial<StandardModifierObject<number>>): StandardModifierKeys[];
    keys(obj: ModifierData): ModifierKeys[];
    keys(obj: ModifierActive): ModifierKeys[];
    keys(obj: Shop): ShopCategory[];
    entries(obj: ModifierActive): ModifierActiveEntry[];
    entries(obj: ModifierData): ModifierDataEntry[];
    entries(
        obj: ModifierObject<SkillModifierTemplate, StandardModifierTemplate>
    ): ModifierEntry<SkillModifierTemplate, StandardModifierTemplate>[];
    entries(obj: CombatModifierData): [keyof CombatModifierObject<number>, number][];
    entries<T>(obj: EquipmentObject<T>): [keyof EquipmentObject<T>, T][];
    entries<T>(obj: NumberDictionary<T>): [string, T][];
}
/** onClick callback function */
declare function setAccount(): void;
/**
 *
 * @param {GameMode} gamemode
 */
declare function setupGamemode(gamemode: GameMode): void;
/** onClick callback function (that is being used as href) */
declare function showUsernameChange(): void;
/** onclick callback function */
declare function changeName(): void;
declare function accountDeletion(confirmation?: boolean, characterID?: number, characterName?: string): void;
declare function gameUpdate(): void;
/** Save game variable */
declare var firstTime: number;
/** Save game variable */
declare var nameChanges: number;
/** Save game variable */
declare var username: string;
declare const previousGameVersion: 'Alpha v0.20';
declare const characterSelectAnnouncementVersion: 2;
/** Save game variable */
declare var gameUpdateNotification: string;
/** Save game variable */
declare var accountGameVersion: number;
/** @type {NumberDictionary<GamemodeData>} */
declare const GAMEMODES: NumberDictionary<GamemodeData>;
/** @type {(keyof SaveGame)[]} */
declare var allVars: (keyof SaveGame)[];
/** @type {(keyof SaveGame)[]} */
declare const importantSaveVars: (keyof SaveGame)[];
declare namespace CONSTANTS {
    namespace page {
        const Woodcutting: number;
        const Shop: number;
        const Bank: number;
        const Settings: number;
        const Skills: number;
        const Statistics: number;
        const Fishing: number;
        const Firemaking: number;
        const Cooking: number;
        const Mining: number;
        const Smithing: number;
        const Mastery: number;
        const Combat: number;
        const Thieving: number;
        const Farming: number;
        const Fletching: number;
        const Crafting: number;
        const Runecrafting: number;
        const Herblore: number;
        const Archaeology: number;
        const Easter: number;
        const CaseOfFortune: number;
        const AltMagic: number;
        const GolbinRaid: number;
        const Xmas2020: number;
        const Agility: number;
        const Corruption: number;
        const Summoning: number;
    }
    const skill: SkillObject<number>;
    namespace shop {
        namespace general {
            const Extra_Bank_Slot: number;
            const Auto_Eat_Tier_I: number;
            const Auto_Eat_Tier_II: number;
            const Auto_Eat_Tier_III: number;
            const Extra_Equipment_Set_I: number;
            const Extra_Equipment_Set_II: number;
            const Dungeon_Equipment_Swapping: number;
            const Multi_Tree: number;
        }
        namespace skillUpgrades {
            const Iron_Axe: number;
            const Steel_Axe: number;
            const Black_Axe: number;
            const Mithril_Axe: number;
            const Adamant_Axe: number;
            const Rune_Axe: number;
            const Dragon_Axe: number;
            const Iron_Fishing_Rod: number;
            const Steel_Fishing_Rod: number;
            const Black_Fishing_Rod: number;
            const Mithril_Fishing_Rod: number;
            const Adamant_Fishing_Rod: number;
            const Rune_Fishing_Rod: number;
            const Dragon_Fishing_Rod: number;
            const Iron_Pickaxe: number;
            const Steel_Pickaxe: number;
            const Black_Pickaxe: number;
            const Mithril_Pickaxe: number;
            const Adamant_Pickaxe: number;
            const Rune_Pickaxe: number;
            const Dragon_Pickaxe: number;
            const Normal_Cooking_Fire: number;
            const Oak_Cooking_Fire: number;
            const Willow_Cooking_Fire: number;
            const Teak_Cooking_Fire: number;
            const Maple_Cooking_Fire: number;
            const Mahogany_Cooking_Fire: number;
            const Yew_Cooking_Fire: number;
            const Magic_Cooking_Fire: number;
            const Redwood_Cooking_Fire: number;
            const Perpetual_Haste: number;
            const Expanded_Knowledge: number;
            const Master_of_Nature: number;
            const Art_of_Control: number;
        }
        namespace slayer {
            const Auto_Slayer: number;
            const Basic_Resupply: number;
            const Standard_Resupply: number;
            const Generous_Resupply: number;
            const Mirror_Shield: number;
            const Magical_Ring: number;
            const Desert_Hat: number;
            const Blazing_Lantern: number;
            const Climbing_Boots: number;
            const Confetti_Crossbow: number;
            const Skull_Cape: number;
            const Green_Party_Hat: number;
            const Slayer_Helmet_Basic: number;
            const Slayer_Platebody_Basic: number;
            const Slayer_Cowl_Basic: number;
            const Slayer_Leather_Body_Basic: number;
            const Slayer_Wizard_Hat_Basic: number;
            const Slayer_Wizard_Robes_Basic: number;
            const Slayer_Upgrade_Kit_Strong: number;
            const Slayer_Upgrade_Kit_Elite: number;
            const Slayer_Upgrade_Kit_Master: number;
        }
        namespace gloves {
            const Cooking_1: number;
            export { Cooking_1 as Cooking };
            const Mining_1: number;
            export { Mining_1 as Mining };
            const Smithing_1: number;
            export { Smithing_1 as Smithing };
            const Thieving_1: number;
            export { Thieving_1 as Thieving };
            export const Gem: number;
        }
        namespace skillcapes {
            const Max_Skillcape: number;
            const Agility_Skillcape: number;
            const Attack_Skillcape: number;
            const Cooking_Skillcape: number;
            const Crafting_Skillcape: number;
            const Defence_Skillcape: number;
            const Farming_Skillcape: number;
            const Firemaking_Skillcape: number;
            const Fishing_Skillcape: number;
            const Fletching_Skillcape: number;
            const Herblore_Skillcape: number;
            const Hitpoints_Skillcape: number;
            const Magic_Skillcape: number;
            const Mining_Skillcape: number;
            const Prayer_Skillcape: number;
            const Ranged_Skillcape: number;
            const Runecrafting_Skillcape: number;
            const Slayer_Skillcape: number;
            const Smithing_Skillcape: number;
            const Strength_Skillcape: number;
            const Thieving_Skillcape: number;
            const Woodcutting_Skillcape: number;
        }
        namespace materials {
            const Feathers: number;
            const Compost: number;
            const Weird_Gloop: number;
            const Bowstring: number;
            const Leather: number;
            const Green_Dragonhide: number;
            const Blue_Dragonhide: number;
            const Red_Dragonhide: number;
            const Red_Party_Hat: number;
        }
    }
    namespace mastery {
        const Smithing_2: NumberDictionary<string>;
        export { Smithing_2 as Smithing };
    }
    namespace attackType {
        const Melee: number;
        const Ranged: number;
        const Magic: number;
    }
    namespace attackStyle {
        export const Stab: number;
        export const Slash: number;
        export const Block: number;
        export const Accurate: number;
        export const Rapid: number;
        export const Longrange: number;
        const Magic_1: number;
        export { Magic_1 as Magic };
        export const Defensive: number;
    }
    namespace equipmentSlot {
        const Helmet: number;
        const Platebody: number;
        const Platelegs: number;
        const Boots: number;
        const Weapon: number;
        const Shield: number;
        const Amulet: number;
        const Ring: number;
        const Gloves: number;
        const Quiver: number;
        const Cape: number;
        const Passive: number;
        const Summon: number;
    }
    namespace axe {
        const Bronze: number;
        const Iron: number;
        const Steel: number;
        const Black: number;
        const Mithril: number;
        const Adamant: number;
        const Rune: number;
        const Dragon: number;
    }
    namespace fishingRod {
        const Bronze_1: number;
        export { Bronze_1 as Bronze };
        const Iron_1: number;
        export { Iron_1 as Iron };
        const Steel_1: number;
        export { Steel_1 as Steel };
        const Black_1: number;
        export { Black_1 as Black };
        const Mithril_1: number;
        export { Mithril_1 as Mithril };
        const Adamant_1: number;
        export { Adamant_1 as Adamant };
        const Rune_1: number;
        export { Rune_1 as Rune };
        const Dragon_1: number;
        export { Dragon_1 as Dragon };
    }
    namespace pickaxe {
        const Bronze_2: number;
        export { Bronze_2 as Bronze };
        const Iron_2: number;
        export { Iron_2 as Iron };
        const Steel_2: number;
        export { Steel_2 as Steel };
        const Black_2: number;
        export { Black_2 as Black };
        const Mithril_2: number;
        export { Mithril_2 as Mithril };
        const Adamant_2: number;
        export { Adamant_2 as Adamant };
        const Rune_2: number;
        export { Rune_2 as Rune };
        const Dragon_2: number;
        export { Dragon_2 as Dragon };
    }
    namespace combatArea {
        const Farmlands: number;
        const Dragon_Valley: number;
        const Wet_Forest: number;
        const Wizard_Tower: number;
        const Castle_of_Kings: number;
        const Bandit_Hideout: number;
        const Giant_Dungeon: number;
        const Sandy_Shores: number;
        const Icy_Hills: number;
        const Goblin_Village: number;
        const Graveyard: number;
    }
    namespace slayerArea {
        const Penumbra: number;
        const Strange_Cave: number;
        const High_Lands: number;
        const Holy_Isles: number;
        const Forest_of_Goo: number;
        const Desolate_Plains: number;
        const Runic_Ruins: number;
        const Arid_Plains: number;
        const Shrouded_Badlands: number;
        const Perilous_Peaks: number;
        const Dark_Waters: number;
    }
    namespace dungeon {
        const Chicken_Coop: number;
        const Undead_Graveyard: number;
        const Spider_Forest: number;
        const Frozen_Cove: number;
        const Deep_Sea_Ship: number;
        const Volcanic_Cave: number;
        const Bandit_Base: number;
        const Hall_of_Wizards: number;
        const Air_God_Dungeon: number;
        const Water_God_Dungeon: number;
        const Earth_God_Dungeon: number;
        const Fire_God_Dungeon: number;
        const Dragons_Den: number;
        const Miolite_Caves: number;
        const Infernal_Stronghold: number;
        const Into_the_Mist: number;
    }
    namespace spellType {
        const Air: number;
        const Water: number;
        const Earth: number;
        const Fire: number;
    }
    namespace specialEvent {
        const aprilFools2020: number;
        const easter2020: number;
        const christmas2020: number;
    }
    namespace bones {
        const Bones: number;
        const Dragon_Bones: number;
        const Magic_Bones: number;
    }
    namespace spell {
        const Wind_Strike: number;
        const Water_Strike: number;
        const Earth_Strike: number;
        const Fire_Strike: number;
        const Wind_Bolt: number;
        const Water_Bolt: number;
        const Earth_Bolt: number;
        const Fire_Bolt: number;
        const Wind_Blast: number;
        const Water_Blast: number;
        const Earth_Blast: number;
        const Fire_Blast: number;
        const Wind_Wave: number;
        const Water_Wave: number;
        const Earth_Wave: number;
        const Fire_Wave: number;
        const Wind_Surge: number;
        const Water_Surge: number;
        const Earth_Surge: number;
        const Fire_Surge: number;
    }
    namespace curse {
        const Blinding_I: number;
        const Soul_Split_I: number;
        const Weakening_I: number;
        const Anguish_I: number;
        const Blinding_II: number;
        const Soul_Split_II: number;
        const Weakening_II: number;
        const Confusion: number;
        const Anguish_II: number;
        const Blinding_III: number;
        const Soul_Split_III: number;
        const Weakening_III: number;
        const Anguish_III: number;
        const Decay: number;
    }
    namespace aurora {
        const Surge_I: number;
        const Fury_I: number;
        const Fervor_I: number;
        const Surge_II: number;
        const Charged_I: number;
        const Fury_II: number;
        const Fervor_II: number;
        const Surge_III: number;
        const Charged_II: number;
        const Fury_III: number;
        const Fervor_III: number;
        const Charged_III: number;
    }
    namespace prayer {
        const Thick_Skin: number;
        const Burst_of_Strength: number;
        const Clarity_of_Thought: number;
        const Sharp_Eye: number;
        const Mystic_Will: number;
        const Rock_Skin: number;
        const Superhuman_Strength: number;
        const Improved_Reflexes: number;
        const Rapid_Heal: number;
        const Protect_Item: number;
        const Hawk_Eye: number;
        const Mystic_Lore: number;
        const Steel_Skin: number;
        const Ultimate_Strength: number;
        const Incredible_Reflexes: number;
        const Protect_from_Magic: number;
        const Protect_from_Ranged: number;
        const Protect_from_Melee: number;
        const Eagle_Eye: number;
        const Mystic_Might: number;
        const Redemption: number;
        const Chivalry: number;
        const Piety: number;
        const Rigour: number;
        const Augury: number;
    }
    namespace slayerTier {
        const Easy: number;
        const Normal: number;
        const Hard: number;
        const Elite: number;
        const Master: number;
    }
    namespace gamemode {
        const Standard: number;
        const Hardcore: number;
        const Adventure: number;
        const Chaos: number;
    }
    namespace item {
        export const Normal_Logs: number;
        export const Oak_Logs: number;
        export const Willow_Logs: number;
        export const Teak_Logs: number;
        export const Maple_Logs: number;
        export const Mahogany_Logs: number;
        export const Yew_Logs: number;
        export const Magic_Logs: number;
        export const Redwood_Logs: number;
        export const Raw_Shrimp: number;
        export const Raw_Sardine: number;
        export const Raw_Herring: number;
        export const Raw_Trout: number;
        export const Raw_Salmon: number;
        export const Raw_Lobster: number;
        export const Raw_Swordfish: number;
        export const Raw_Crab: number;
        export const Raw_Shark: number;
        export const Raw_Cave_Fish: number;
        export const Raw_Manta_Ray: number;
        export const Raw_Whale: number;
        export const Shrimp: number;
        export const Sardine: number;
        export const Herring: number;
        export const Trout: number;
        export const Salmon: number;
        export const Lobster: number;
        export const Swordfish: number;
        export const Crab: number;
        export const Shark: number;
        export const Cave_Fish: number;
        export const Manta_Ray: number;
        export const Whale: number;
        export const Burnt_Shrimp: number;
        export const Burnt_Sardine: number;
        export const Burnt_Herring: number;
        export const Burnt_Trout: number;
        export const Burnt_Salmon: number;
        export const Burnt_Lobster: number;
        export const Burnt_Swordfish: number;
        export const Burnt_Crab: number;
        export const Burnt_Shark: number;
        export const Burnt_Cave_Fish: number;
        export const Burnt_Manta_Ray: number;
        export const Burnt_Whale: number;
        export const Copper_Ore: number;
        export const Tin_Ore: number;
        export const Iron_Ore: number;
        export const Coal_Ore: number;
        export const Silver_Ore: number;
        export const Gold_Ore: number;
        export const Mithril_Ore: number;
        export const Adamantite_Ore: number;
        export const Runite_Ore: number;
        export const Dragonite_Ore: number;
        export const Bronze_Bar: number;
        export const Iron_Bar: number;
        export const Steel_Bar: number;
        export const Gold_Bar: number;
        export const Mithril_Bar: number;
        export const Adamantite_Bar: number;
        export const Runite_Bar: number;
        export const Dragonite_Bar: number;
        export const Bronze_Dagger: number;
        export const Bronze_Sword: number;
        export const Bronze_Battleaxe: number;
        export const Bronze_2H_Sword: number;
        export const Bronze_Helmet: number;
        export const Bronze_Boots: number;
        export const Bronze_Platelegs: number;
        export const Bronze_Platebody: number;
        export const Iron_Dagger: number;
        export const Iron_Sword: number;
        export const Iron_Battleaxe: number;
        export const Iron_2H_Sword: number;
        export const Iron_Helmet: number;
        export const Iron_Boots: number;
        export const Iron_Platelegs: number;
        export const Iron_Platebody: number;
        export const Steel_Dagger: number;
        export const Steel_Sword: number;
        export const Steel_Battleaxe: number;
        export const Steel_2H_Sword: number;
        export const Steel_Helmet: number;
        export const Steel_Boots: number;
        export const Steel_Platelegs: number;
        export const Steel_Platebody: number;
        export const Mithril_Dagger: number;
        export const Mithril_Sword: number;
        export const Mithril_Battleaxe: number;
        export const Mithril_2H_Sword: number;
        export const Mithril_Helmet: number;
        export const Mithril_Boots: number;
        export const Mithril_Platelegs: number;
        export const Mithril_Platebody: number;
        export const Adamant_Dagger: number;
        export const Adamant_Sword: number;
        export const Adamant_Battleaxe: number;
        export const Adamant_2H_Sword: number;
        export const Adamant_Helmet: number;
        export const Adamant_Boots: number;
        export const Adamant_Platelegs: number;
        export const Adamant_Platebody: number;
        export const Rune_Dagger: number;
        export const Rune_Sword: number;
        export const Rune_Battleaxe: number;
        export const Rune_2H_Sword: number;
        export const Rune_Helmet: number;
        export const Rune_Boots: number;
        export const Rune_Platelegs: number;
        export const Rune_Platebody: number;
        export const Dragon_Dagger: number;
        export const Dragon_Sword: number;
        export const Dragon_Battleaxe: number;
        export const Dragon_2H_Sword: number;
        export const Dragon_Helmet: number;
        export const Dragon_Boots: number;
        export const Dragon_Platelegs: number;
        export const Dragon_Platebody: number;
        export const Bird_Nest: number;
        export const Treasure_Chest: number;
        export const Bronze_Shield: number;
        export const Iron_Shield: number;
        export const Steel_Shield: number;
        export const Mithril_Shield: number;
        export const Adamant_Shield: number;
        export const Rune_Shield: number;
        export const Dragon_Shield: number;
        export const Topaz: number;
        export const Sapphire: number;
        export const Ruby: number;
        export const Emerald: number;
        export const Diamond: number;
        export const Silver_Bar: number;
        export const Black_Dagger: number;
        export const Black_Sword: number;
        export const Black_Battleaxe: number;
        export const Black_2H_Sword: number;
        export const Black_Helmet: number;
        export const Black_Boots: number;
        export const Black_Platelegs: number;
        export const Black_Platebody: number;
        export const Black_Shield: number;
        export const Potato_Seed: number;
        export const Onion_Seed: number;
        export const Cabbage_Seed: number;
        export const Tomato_Seed: number;
        export const Sweetcorn_Seed: number;
        export const Strawberry_Seed: number;
        export const Watermelon_Seed: number;
        export const Snape_Grass_Seed: number;
        export const Potatoes: number;
        export const Onions: number;
        export const Cabbage: number;
        export const Tomatoes: number;
        export const Sweetcorn: number;
        export const Strawberries: number;
        export const Watermelon: number;
        export const Snape_Grass: number;
        const Compost_1: number;
        export { Compost_1 as Compost };
        export const Oak_Tree_Seed: number;
        export const Willow_Tree_Seed: number;
        export const Maple_Tree_Seed: number;
        export const Yew_Tree_Seed: number;
        export const Magic_Tree_Seed: number;
        export const Bronze_Helmet_T_S: number;
        export const Bronze_Boots_T_S: number;
        export const Bronze_Platelegs_T_S: number;
        export const Bronze_Platebody_T_S: number;
        export const Bronze_Shield_T_S: number;
        export const Iron_Helmet_T_S: number;
        export const Iron_Boots_T_S: number;
        export const Iron_Platelegs_T_S: number;
        export const Iron_Platebody_T_S: number;
        export const Iron_Shield_T_S: number;
        export const Steel_Helmet_T_S: number;
        export const Steel_Boots_T_S: number;
        export const Steel_Platelegs_T_S: number;
        export const Steel_Platebody_T_S: number;
        export const Steel_Shield_T_S: number;
        export const Black_Helmet_T_S: number;
        export const Black_Boots_T_S: number;
        export const Black_Platelegs_T_S: number;
        export const Black_Platebody_T_S: number;
        export const Black_Shield_T_S: number;
        export const Mithril_Helmet_T_S: number;
        export const Mithril_Boots_T_S: number;
        export const Mithril_Platelegs_T_S: number;
        export const Mithril_Platebody_T_S: number;
        export const Mithril_Shield_T_S: number;
        export const Adamant_Helmet_T_S: number;
        export const Adamant_Boots_T_S: number;
        export const Adamant_Platelegs_T_S: number;
        export const Adamant_Platebody_T_S: number;
        export const Adamant_Shield_T_S: number;
        export const Rune_Helmet_T_S: number;
        export const Rune_Boots_T_S: number;
        export const Rune_Platelegs_T_S: number;
        export const Rune_Platebody_T_S: number;
        export const Rune_Shield_T_S: number;
        export const Dragon_Helmet_T_S: number;
        export const Dragon_Boots_T_S: number;
        export const Dragon_Platelegs_T_S: number;
        export const Dragon_Platebody_T_S: number;
        export const Dragon_Shield_T_S: number;
        export const Bronze_Helmet_T_G: number;
        export const Bronze_Boots_T_G: number;
        export const Bronze_Platelegs_T_G: number;
        export const Bronze_Platebody_T_G: number;
        export const Bronze_Shield_T_G: number;
        export const Iron_Helmet_T_G: number;
        export const Iron_Boots_T_G: number;
        export const Iron_Platelegs_T_G: number;
        export const Iron_Platebody_T_G: number;
        export const Iron_Shield_T_G: number;
        export const Steel_Helmet_T_G: number;
        export const Steel_Boots_T_G: number;
        export const Steel_Platelegs_T_G: number;
        export const Steel_Platebody_T_G: number;
        export const Steel_Shield_T_G: number;
        export const Black_Helmet_T_G: number;
        export const Black_Boots_T_G: number;
        export const Black_Platelegs_T_G: number;
        export const Black_Platebody_T_G: number;
        export const Black_Shield_T_G: number;
        export const Mithril_Helmet_T_G: number;
        export const Mithril_Boots_T_G: number;
        export const Mithril_Platelegs_T_G: number;
        export const Mithril_Platebody_T_G: number;
        export const Mithril_Shield_T_G: number;
        export const Adamant_Helmet_T_G: number;
        export const Adamant_Boots_T_G: number;
        export const Adamant_Platelegs_T_G: number;
        export const Adamant_Platebody_T_G: number;
        export const Adamant_Shield_T_G: number;
        export const Rune_Helmet_T_G: number;
        export const Rune_Boots_T_G: number;
        export const Rune_Platelegs_T_G: number;
        export const Rune_Platebody_T_G: number;
        export const Rune_Shield_T_G: number;
        export const Dragon_Helmet_T_G: number;
        export const Dragon_Boots_T_G: number;
        export const Dragon_Platelegs_T_G: number;
        export const Dragon_Platebody_T_G: number;
        export const Dragon_Shield_T_G: number;
        export const Amulet_of_Fishing: number;
        export const Amulet_of_Strength: number;
        export const Amulet_of_Accuracy: number;
        export const Amulet_of_Defence: number;
        export const Amulet_of_Glory: number;
        export const Normal_Shortbow: number;
        export const Oak_Shortbow: number;
        export const Willow_Shortbow: number;
        export const Maple_Shortbow: number;
        export const Yew_Shortbow: number;
        export const Magic_Shortbow: number;
        export const Normal_Longbow: number;
        export const Oak_Longbow: number;
        export const Willow_Longbow: number;
        export const Maple_Longbow: number;
        export const Yew_Longbow: number;
        export const Magic_Longbow: number;
        export const Bronze_Arrows: number;
        export const Iron_Arrows: number;
        export const Steel_Arrows: number;
        export const Mithril_Arrows: number;
        export const Adamant_Arrows: number;
        export const Rune_Arrows: number;
        export const Dragon_Arrows: number;
        export const Bronze_Arrowtips: number;
        export const Iron_Arrowtips: number;
        export const Steel_Arrowtips: number;
        export const Mithril_Arrowtips: number;
        export const Adamant_Arrowtips: number;
        export const Rune_Arrowtips: number;
        export const Dragon_Arrowtips: number;
        export const Arrow_Shafts: number;
        export const Headless_Arrows: number;
        const Feathers_1: number;
        export { Feathers_1 as Feathers };
        export const Normal_Shortbow_U: number;
        export const Oak_Shortbow_U: number;
        export const Willow_Shortbow_U: number;
        export const Maple_Shortbow_U: number;
        export const Yew_Shortbow_U: number;
        export const Magic_Shortbow_U: number;
        export const Normal_Longbow_U: number;
        export const Oak_Longbow_U: number;
        export const Willow_Longbow_U: number;
        export const Maple_Longbow_U: number;
        export const Yew_Longbow_U: number;
        export const Magic_Longbow_U: number;
        const Bowstring_1: number;
        export { Bowstring_1 as Bowstring };
        const Leather_1: number;
        export { Leather_1 as Leather };
        const Green_Dragonhide_1: number;
        export { Green_Dragonhide_1 as Green_Dragonhide };
        const Blue_Dragonhide_1: number;
        export { Blue_Dragonhide_1 as Blue_Dragonhide };
        const Red_Dragonhide_1: number;
        export { Red_Dragonhide_1 as Red_Dragonhide };
        export const Black_Dragonhide: number;
        export const Leather_Gloves: number;
        export const Leather_Boots: number;
        export const Leather_Cowl: number;
        export const Leather_Vambraces: number;
        export const Leather_Body: number;
        export const Leather_Chaps: number;
        export const Green_Dhide_Vambraces: number;
        export const Green_Dhide_Chaps: number;
        export const Green_Dhide_Body: number;
        export const Blue_Dhide_Vambraces: number;
        export const Blue_Dhide_Chaps: number;
        export const Blue_Dhide_Body: number;
        export const Red_Dhide_Vambraces: number;
        export const Red_Dhide_Chaps: number;
        export const Red_Dhide_Body: number;
        export const Black_Dhide_Vambraces: number;
        export const Black_Dhide_Chaps: number;
        export const Black_Dhide_Body: number;
        export const Silver_Topaz_Ring: number;
        export const Silver_Sapphire_Ring: number;
        export const Silver_Ruby_Ring: number;
        export const Silver_Emerald_Ring: number;
        export const Silver_Diamond_Ring: number;
        export const Gold_Topaz_Ring: number;
        export const Gold_Sapphire_Ring: number;
        export const Gold_Ruby_Ring: number;
        export const Gold_Emerald_Ring: number;
        export const Gold_Diamond_Ring: number;
        export const Silver_Topaz_Necklace: number;
        export const Silver_Sapphire_Necklace: number;
        export const Silver_Ruby_Necklace: number;
        export const Silver_Emerald_Necklace: number;
        export const Silver_Diamond_Necklace: number;
        export const Gold_Topaz_Necklace: number;
        export const Gold_Sapphire_Necklace: number;
        export const Gold_Ruby_Necklace: number;
        export const Gold_Emerald_Necklace: number;
        export const Gold_Diamond_Necklace: number;
        export const Cooking_Gloves: number;
        export const Mining_Gloves: number;
        export const Smithing_Gloves: number;
        export const Thieving_Gloves: number;
        export const Gem_Gloves: number;
        export const Cape_Of_Prat: number;
        export const Obsidian_Cape: number;
        export const Elite_Amulet_of_Strength: number;
        export const Elite_Amulet_of_Accuracy: number;
        export const Elite_Amulet_of_Defence: number;
        export const Elite_Amulet_of_Glory: number;
        export const Egg_Chest: number;
        export const Ancient_Sword: number;
        export const Ancient_Helmet: number;
        export const Ancient_Platelegs: number;
        export const Ancient_Platebody: number;
        export const Ancient_Shield: number;
        export const Ancient_Helmet_T_S: number;
        export const Ancient_Platelegs_T_S: number;
        export const Ancient_Platebody_T_S: number;
        export const Ancient_Shield_T_S: number;
        export const Ancient_Helmet_T_G: number;
        export const Ancient_Platelegs_T_G: number;
        export const Ancient_Platebody_T_G: number;
        export const Ancient_Shield_T_G: number;
        export const Pirate_Booty: number;
        export const Fire_Cape: number;
        export const Elite_Chest: number;
        export const Spider_Chest: number;
        export const Rangers_Hat: number;
        export const Ranger_Boots: number;
        export const Amulet_of_Fury: number;
        export const Amulet_of_Torture: number;
        export const Amulet_of_Ranged: number;
        export const Ice_Dagger: number;
        export const Ice_Sword: number;
        export const Ice_Battleaxe: number;
        export const Ice_2h_Sword: number;
        export const Ice_Helmet: number;
        export const Ice_Boots: number;
        export const Ice_Platelegs: number;
        export const Ice_Platebody: number;
        export const Ice_Shield: number;
        export const Ice_Arrows: number;
        export const Ice_Shortbow: number;
        export const Ice_Longbow: number;
        export const Frozen_Chest: number;
        export const Standard_Chest: number;
        export const Amulet_of_Looting: number;
        export const Redwood_Shortbow_U: number;
        export const Redwood_Shortbow: number;
        export const Redwood_Longbow_U: number;
        export const Redwood_Longbow: number;
        export const Rune_Essence: number;
        export const Air_Rune: number;
        export const Mind_Rune: number;
        export const Water_Rune: number;
        export const Earth_Rune: number;
        export const Fire_Rune: number;
        export const Body_Rune: number;
        export const Chaos_Rune: number;
        export const Death_Rune: number;
        export const Blood_Rune: number;
        export const Ancient_Rune: number;
        export const Staff_of_Air: number;
        export const Staff_of_Water: number;
        export const Staff_of_Earth: number;
        export const Staff_of_Fire: number;
        export const Air_Battlestaff: number;
        export const Water_Battlestaff: number;
        export const Earth_Battlestaff: number;
        export const Fire_Battlestaff: number;
        export const Mystic_Air_Staff: number;
        export const Mystic_Water_Staff: number;
        export const Mystic_Earth_Staff: number;
        export const Mystic_Fire_Staff: number;
        export const Green_Wizard_Hat: number;
        export const Green_Wizard_Robes: number;
        export const Green_Wizard_Bottoms: number;
        export const Green_Wizard_Boots: number;
        export const Blue_Wizard_Hat: number;
        export const Blue_Wizard_Robes: number;
        export const Blue_Wizard_Bottoms: number;
        export const Blue_Wizard_Boots: number;
        export const Red_Wizard_Hat: number;
        export const Red_Wizard_Robes: number;
        export const Red_Wizard_Bottoms: number;
        export const Red_Wizard_Boots: number;
        export const Black_Wizard_Hat: number;
        export const Black_Wizard_Robes: number;
        export const Black_Wizard_Bottoms: number;
        export const Black_Wizard_Boots: number;
        export const Ancient_Wizard_Hat: number;
        export const Ancient_Wizard_Robes: number;
        export const Ancient_Wizard_Bottoms: number;
        export const Ancient_Wizard_Boots: number;
        export const Bronze_Scimitar: number;
        export const Iron_Scimitar: number;
        export const Steel_Scimitar: number;
        export const Black_Scimitar: number;
        export const Mithril_Scimitar: number;
        export const Adamant_Scimitar: number;
        export const Rune_Scimitar: number;
        export const Dragon_Scimitar: number;
        const Bones_1: number;
        export { Bones_1 as Bones };
        const Dragon_Bones_1: number;
        export { Dragon_Bones_1 as Dragon_Bones };
        const Magic_Bones_1: number;
        export { Magic_Bones_1 as Magic_Bones };
        export const Bandit_Chest: number;
        export const Ancient_Longbow: number;
        const Attack_Skillcape_1: number;
        export { Attack_Skillcape_1 as Attack_Skillcape };
        const Cooking_Skillcape_1: number;
        export { Cooking_Skillcape_1 as Cooking_Skillcape };
        const Crafting_Skillcape_1: number;
        export { Crafting_Skillcape_1 as Crafting_Skillcape };
        const Defence_Skillcape_1: number;
        export { Defence_Skillcape_1 as Defence_Skillcape };
        const Farming_Skillcape_1: number;
        export { Farming_Skillcape_1 as Farming_Skillcape };
        const Firemaking_Skillcape_1: number;
        export { Firemaking_Skillcape_1 as Firemaking_Skillcape };
        const Fishing_Skillcape_1: number;
        export { Fishing_Skillcape_1 as Fishing_Skillcape };
        const Fletching_Skillcape_1: number;
        export { Fletching_Skillcape_1 as Fletching_Skillcape };
        const Hitpoints_Skillcape_1: number;
        export { Hitpoints_Skillcape_1 as Hitpoints_Skillcape };
        const Magic_Skillcape_1: number;
        export { Magic_Skillcape_1 as Magic_Skillcape };
        const Mining_Skillcape_1: number;
        export { Mining_Skillcape_1 as Mining_Skillcape };
        const Ranged_Skillcape_1: number;
        export { Ranged_Skillcape_1 as Ranged_Skillcape };
        const Runecrafting_Skillcape_1: number;
        export { Runecrafting_Skillcape_1 as Runecrafting_Skillcape };
        const Smithing_Skillcape_1: number;
        export { Smithing_Skillcape_1 as Smithing_Skillcape };
        const Strength_Skillcape_1: number;
        export { Strength_Skillcape_1 as Strength_Skillcape };
        const Thieving_Skillcape_1: number;
        export { Thieving_Skillcape_1 as Thieving_Skillcape };
        const Woodcutting_Skillcape_1: number;
        export { Woodcutting_Skillcape_1 as Woodcutting_Skillcape };
        export const Magic_Chest: number;
        export const Bronze_Gloves: number;
        export const Iron_Gloves: number;
        export const Steel_Gloves: number;
        export const Mithril_Gloves: number;
        export const Adamant_Gloves: number;
        export const Rune_Gloves: number;
        export const Dragon_Gloves: number;
        export const Carrot_Seeds: number;
        export const Carrot: number;
        export const Mastery_Token_Cooking: number;
        export const Mastery_Token_Crafting: number;
        export const Mastery_Token_Farming: number;
        export const Mastery_Token_Firemaking: number;
        export const Mastery_Token_Fishing: number;
        export const Mastery_Token_Fletching: number;
        export const Mastery_Token_Mining: number;
        export const Mastery_Token_Runecrafting: number;
        export const Mastery_Token_Smithing: number;
        export const Mastery_Token_Thieving: number;
        export const Mastery_Token_Woodcutting: number;
        export const Bobbys_Pocket: number;
        const Prayer_Skillcape_1: number;
        export { Prayer_Skillcape_1 as Prayer_Skillcape };
        const Slayer_Helmet_Basic_1: number;
        export { Slayer_Helmet_Basic_1 as Slayer_Helmet_Basic };
        const Slayer_Platebody_Basic_1: number;
        export { Slayer_Platebody_Basic_1 as Slayer_Platebody_Basic };
        export const Slayer_Helmet_Strong: number;
        export const Slayer_Platebody_Strong: number;
        export const Slayer_Helmet_Elite: number;
        export const Slayer_Platebody_Elite: number;
        export const Magic_Wand_Basic: number;
        export const Magic_Wand_Powerful: number;
        export const Magic_Wand_Elite: number;
        export const Book_of_Eli: number;
        const Mirror_Shield_1: number;
        export { Mirror_Shield_1 as Mirror_Shield };
        export const Eyeball: number;
        export const Dragon_Claw_Fragment: number;
        export const Dragon_Claw: number;
        export const Ancient_Claw_Fragment: number;
        export const Ancient_Claw: number;
        export const Holy_Dust: number;
        export const Cape_of_Arrow_Preservation: number;
        const Magical_Ring_1: number;
        export { Magical_Ring_1 as Magical_Ring };
        export const Ancient_Arrow: number;
        export const Ancient_2H_Sword: number;
        const Slayer_Skillcape_1: number;
        export { Slayer_Skillcape_1 as Slayer_Skillcape };
        export const Big_Bones: number;
        const Slayer_Wizard_Hat_Basic_1: number;
        export { Slayer_Wizard_Hat_Basic_1 as Slayer_Wizard_Hat_Basic };
        const Slayer_Wizard_Robes_Basic_1: number;
        export { Slayer_Wizard_Robes_Basic_1 as Slayer_Wizard_Robes_Basic };
        export const Slayer_Wizard_Hat_Strong: number;
        export const Slayer_Wizard_Robes_Strong: number;
        export const Slayer_Wizard_Hat_Elite: number;
        export const Slayer_Wizard_Robes_Elite: number;
        const Slayer_Cowl_Basic_1: number;
        export { Slayer_Cowl_Basic_1 as Slayer_Cowl_Basic };
        const Slayer_Leather_Body_Basic_1: number;
        export { Slayer_Leather_Body_Basic_1 as Slayer_Leather_Body_Basic };
        export const Slayer_Cowl_Strong: number;
        export const Slayer_Leather_Body_Strong: number;
        export const Slayer_Cowl_Elite: number;
        export const Slayer_Leather_Body_Elite: number;
        export const Garum_Herb: number;
        export const Sourweed_Herb: number;
        export const Mantalyme_Herb: number;
        export const Lemontyle_Herb: number;
        export const Oxilyme_Herb: number;
        export const Poraxx_Herb: number;
        export const Pigtayle_Herb: number;
        export const Barrentoe_Herb: number;
        export const Garum_Seed: number;
        export const Sourweed_Seed: number;
        export const Mantalyme_Seed: number;
        export const Lemontyle_Seed: number;
        export const Oxilyme_Seed: number;
        export const Poraxx_Seed: number;
        export const Pigtayle_Seed: number;
        export const Barrentoe_Seed: number;
        export const Melee_Accuracy_Potion_I: number;
        export const Melee_Accuracy_Potion_II: number;
        export const Melee_Accuracy_Potion_III: number;
        export const Melee_Accuracy_Potion_IV: number;
        export const Melee_Strength_Potion_I: number;
        export const Melee_Strength_Potion_II: number;
        export const Melee_Strength_Potion_III: number;
        export const Melee_Strength_Potion_IV: number;
        export const Melee_Evasion_Potion_I: number;
        export const Melee_Evasion_Potion_II: number;
        export const Melee_Evasion_Potion_III: number;
        export const Melee_Evasion_Potion_IV: number;
        export const Ranged_Assistance_Potion_I: number;
        export const Ranged_Assistance_Potion_II: number;
        export const Ranged_Assistance_Potion_III: number;
        export const Ranged_Assistance_Potion_IV: number;
        export const Magic_Assistance_Potion_I: number;
        export const Magic_Assistance_Potion_II: number;
        export const Magic_Assistance_Potion_III: number;
        export const Magic_Assistance_Potion_IV: number;
        export const Regeneration_Potion_I: number;
        export const Regeneration_Potion_II: number;
        export const Regeneration_Potion_III: number;
        export const Regeneration_Potion_IV: number;
        export const Damage_Reduction_Potion_I: number;
        export const Damage_Reduction_Potion_II: number;
        export const Damage_Reduction_Potion_III: number;
        export const Damage_Reduction_Potion_IV: number;
        export const Bird_Nest_Potion_I: number;
        export const Bird_Nest_Potion_II: number;
        export const Bird_Nest_Potion_III: number;
        export const Bird_Nest_Potion_IV: number;
        export const Fishermans_Potion_I: number;
        export const Fishermans_Potion_II: number;
        export const Fishermans_Potion_III: number;
        export const Fishermans_Potion_IV: number;
        export const Controlled_Heat_Potion_I: number;
        export const Controlled_Heat_Potion_II: number;
        export const Controlled_Heat_Potion_III: number;
        export const Controlled_Heat_Potion_IV: number;
        export const Generous_Cook_Potion_I: number;
        export const Generous_Cook_Potion_II: number;
        export const Generous_Cook_Potion_III: number;
        export const Generous_Cook_Potion_IV: number;
        export const Perfect_Swing_Potion_I: number;
        export const Perfect_Swing_Potion_II: number;
        export const Perfect_Swing_Potion_III: number;
        export const Perfect_Swing_Potion_IV: number;
        export const Seeing_Gold_Potion_I: number;
        export const Seeing_Gold_Potion_II: number;
        export const Seeing_Gold_Potion_III: number;
        export const Seeing_Gold_Potion_IV: number;
        export const Gentle_Hands_Potion_I: number;
        export const Gentle_Hands_Potion_II: number;
        export const Gentle_Hands_Potion_III: number;
        export const Gentle_Hands_Potion_IV: number;
        export const Farming_Potion_I: number;
        export const Farming_Potion_II: number;
        export const Farming_Potion_III: number;
        export const Farming_Potion_IV: number;
        export const Fletching_Potion_I: number;
        export const Fletching_Potion_II: number;
        export const Fletching_Potion_III: number;
        export const Fletching_Potion_IV: number;
        export const Crafting_Potion_I: number;
        export const Crafting_Potion_II: number;
        export const Crafting_Potion_III: number;
        export const Crafting_Potion_IV: number;
        export const Elemental_Potion_I: number;
        export const Elemental_Potion_II: number;
        export const Elemental_Potion_III: number;
        export const Elemental_Potion_IV: number;
        export const Herblore_Potion_I: number;
        export const Herblore_Potion_II: number;
        export const Herblore_Potion_III: number;
        export const Herblore_Potion_IV: number;
        export const Ranged_Strength_Potion_I: number;
        export const Ranged_Strength_Potion_II: number;
        export const Ranged_Strength_Potion_III: number;
        export const Ranged_Strength_Potion_IV: number;
        export const Magic_Damage_Potion_I: number;
        export const Magic_Damage_Potion_II: number;
        export const Magic_Damage_Potion_III: number;
        export const Magic_Damage_Potion_IV: number;
        export const Large_Horn: number;
        export const Herb_Sack: number;
        export const Sunset_Rapier: number;
        export const Chest_of_Witwix: number;
        export const Amulet_of_Calculated_Promotion: number;
        export const Hard_Leather_Gloves: number;
        export const Hard_Leather_Boots: number;
        export const Hard_Leather_Cowl: number;
        export const Hard_Leather_Vambraces: number;
        export const Hard_Leather_Body: number;
        export const Hard_Leather_Chaps: number;
        export const Chapeau_Noir: number;
        export const Mastery_Token_Herblore: number;
        const Herblore_Skillcape_1: number;
        export { Herblore_Skillcape_1 as Herblore_Skillcape };
        export const Diamond_Luck_Potion_I: number;
        export const Diamond_Luck_Potion_II: number;
        export const Diamond_Luck_Potion_III: number;
        export const Diamond_Luck_Potion_IV: number;
        export const Divine_Potion_I: number;
        export const Divine_Potion_II: number;
        export const Divine_Potion_III: number;
        export const Divine_Potion_IV: number;
        export const Lucky_Herb_Potion_I: number;
        export const Lucky_Herb_Potion_II: number;
        export const Lucky_Herb_Potion_III: number;
        export const Lucky_Herb_Potion_IV: number;
        export const Signet_Ring_Half_A: number;
        export const Signet_Ring_Half_B: number;
        export const Aorpheats_Signet_Ring: number;
        export const Old_Boot: number;
        export const Old_Hat: number;
        export const Seaweed: number;
        export const Rusty_Key: number;
        export const Shell: number;
        export const Rope: number;
        export const Glass_Bottle: number;
        export const Rubber_Ducky: number;
        export const Raw_Blowfish: number;
        export const Raw_Poison_Fish: number;
        export const Leaping_Trout: number;
        export const Leaping_Salmon: number;
        export const Leaping_Broad_Fish: number;
        export const Raw_Magic_Fish: number;
        export const Raw_Anglerfish: number;
        export const Raw_Fanfish: number;
        export const Raw_Seahorse: number;
        export const Raw_Carp: number;
        export const Raw_Skeleton_Fish: number;
        export const Pirates_Lost_Ring: number;
        export const Message_In_A_Bottle: number;
        export const Barbarian_Gloves: number;
        export const Ancient_Ring_Of_Skills: number;
        export const Anglerfish: number;
        export const Fanfish: number;
        export const Seahorse: number;
        export const Carp: number;
        export const Burnt_Anglerfish: number;
        export const Burnt_Fanfish: number;
        export const Burnt_Seahorse: number;
        export const Burnt_Carp: number;
        const Weird_Gloop_1: number;
        export { Weird_Gloop_1 as Weird_Gloop };
        export const Clue_Chasers_Insignia: number;
        export const Lemon: number;
        export const Lemons: number;
        export const Lemonade: number;
        export const Topaz_Bolts: number;
        export const Sapphire_Bolts: number;
        export const Ruby_Bolts: number;
        export const Emerald_Bolts: number;
        export const Diamond_Bolts: number;
        export const Bronze_Crossbow: number;
        export const Iron_Crossbow: number;
        export const Steel_Crossbow: number;
        export const Mithril_Crossbow: number;
        export const Adamant_Crossbow: number;
        export const Rune_Crossbow: number;
        export const Dragon_Crossbow: number;
        export const Ancient_Crossbow: number;
        export const Bronze_Javelin: number;
        export const Iron_Javelin: number;
        export const Steel_Javelin: number;
        export const Mithril_Javelin: number;
        export const Adamant_Javelin: number;
        export const Rune_Javelin: number;
        export const Dragon_Javelin: number;
        export const Ancient_Javelin: number;
        export const Bronze_Throwing_Knife: number;
        export const Iron_Throwing_Knife: number;
        export const Steel_Throwing_Knife: number;
        export const Mithril_Throwing_Knife: number;
        export const Adamant_Throwing_Knife: number;
        export const Rune_Throwing_Knife: number;
        export const Dragon_Throwing_Knife: number;
        export const Ancient_Throwing_Knife: number;
        export const Aeris_God_Helmet: number;
        export const Aeris_God_Platelegs: number;
        export const Aeris_God_Platebody: number;
        export const Aeris_God_Boots: number;
        export const Aeris_God_Gloves: number;
        export const Glacia_God_Helmet: number;
        export const Glacia_God_Platelegs: number;
        export const Glacia_God_Platebody: number;
        export const Glacia_God_Boots: number;
        export const Glacia_God_Gloves: number;
        export const Headless_Bolts: number;
        export const Bronze_Crossbow_Head: number;
        export const Iron_Crossbow_Head: number;
        export const Steel_Crossbow_Head: number;
        export const Mithril_Crossbow_Head: number;
        export const Adamant_Crossbow_Head: number;
        export const Rune_Crossbow_Head: number;
        export const Dragon_Crossbow_Head: number;
        export const Bronze_Javelin_Heads: number;
        export const Iron_Javelin_Heads: number;
        export const Steel_Javelin_Heads: number;
        export const Mithril_Javelin_Heads: number;
        export const Adamant_Javelin_Heads: number;
        export const Rune_Javelin_Heads: number;
        export const Dragon_Javelin_Heads: number;
        export const Green_Dhide_Vambraces_U: number;
        export const Green_Dhide_Chaps_U: number;
        export const Green_Dhide_Body_U: number;
        export const Blue_Dhide_Vambraces_U: number;
        export const Blue_Dhide_Chaps_U: number;
        export const Blue_Dhide_Body_U: number;
        export const Red_Dhide_Vambraces_U: number;
        export const Red_Dhide_Chaps_U: number;
        export const Red_Dhide_Body_U: number;
        export const Black_Dhide_Vambraces_U: number;
        export const Black_Dhide_Chaps_U: number;
        export const Black_Dhide_Body_U: number;
        export const Ancient_Dhide_Vambraces: number;
        export const Ancient_Dhide_Chaps: number;
        export const Ancient_Dhide_Body: number;
        export const Ancient_Dhide_Vambraces_U: number;
        export const Ancient_Dhide_Chaps_U: number;
        export const Ancient_Dhide_Body_U: number;
        export const Elite_Amulet_of_Ranged: number;
        export const Elder_Dragonhide: number;
        export const Green_Dhide_Shield: number;
        export const Blue_Dhide_Shield: number;
        export const Red_Dhide_Shield: number;
        export const Black_Dhide_Shield: number;
        export const Ancient_Dhide_Shield: number;
        export const Green_Dhide_Shield_U: number;
        export const Blue_Dhide_Shield_U: number;
        export const Red_Dhide_Shield_U: number;
        export const Black_Dhide_Shield_U: number;
        export const Ancient_Dhide_Shield_U: number;
        export const Air_Shard: number;
        export const Water_Shard: number;
        export const Earth_Shard: number;
        export const Fire_Shard: number;
        export const Air_Chest: number;
        export const Water_Chest: number;
        export const Earth_Chest: number;
        export const Fire_Chest: number;
        export const Terran_God_Helmet: number;
        export const Terran_God_Platelegs: number;
        export const Terran_God_Platebody: number;
        export const Terran_God_Boots: number;
        export const Terran_God_Gloves: number;
        export const Ragnar_God_Helmet: number;
        export const Ragnar_God_Platelegs: number;
        export const Ragnar_God_Platebody: number;
        export const Ragnar_God_Boots: number;
        export const Ragnar_God_Gloves: number;
        export const Deadeye_Ring: number;
        export const Deadeye_Amulet: number;
        export const Scroll_of_Aeris: number;
        export const Scroll_of_Glacia: number;
        export const Scroll_of_Terran: number;
        export const Scroll_of_Ragnar: number;
        export const Warlock_Ring: number;
        export const Warlock_Amulet: number;
        export const Guardian_Ring: number;
        export const Guardian_Amulet: number;
        export const Fighter_Ring: number;
        export const Fighter_Amulet: number;
        export const Aeris_Godsword: number;
        export const Glacia_Godsword: number;
        export const Terran_Godsword: number;
        export const Ragnar_Godsword: number;
        export const Bank_Slot_Token: number;
        export const Stormsnap: number;
        export const Big_Ron: number;
        const Confetti_Crossbow_1: number;
        export { Confetti_Crossbow_1 as Confetti_Crossbow };
        export const Slayer_Crossbow: number;
        export const Slayer_Crossbow_Head: number;
        export const Eight: number;
        export const Twin_Exiles: number;
        const Max_Skillcape_1: number;
        export { Max_Skillcape_1 as Max_Skillcape };
        export const Bobs_Rake: number;
        export const Earth_Layered_Shield: number;
        export const Elder_Chest: number;
        export const Cloudburst_Staff: number;
        export const Amulet_of_Magic: number;
        export const Elite_Amulet_of_Magic: number;
        export const Bone_Necklace: number;
        const Skull_Cape_1: number;
        export { Skull_Cape_1 as Skull_Cape };
        export const Fury_of_the_Elemental_Zodiac: number;
        export const Light_Rune: number;
        export const Nature_Rune: number;
        export const Havoc_Rune: number;
        export const Spirit_Rune: number;
        export const Mist_Rune: number;
        export const Dust_Rune: number;
        export const Mud_Rune: number;
        export const Smoke_Rune: number;
        export const Steam_Rune: number;
        export const Lava_Rune: number;
        export const Air_Acolyte_Wizard_Hat: number;
        export const Air_Acolyte_Wizard_Robes: number;
        export const Air_Acolyte_Wizard_Bottoms: number;
        export const Air_Acolyte_Wizard_Boots: number;
        export const Water_Acolyte_Wizard_Hat: number;
        export const Water_Acolyte_Wizard_Robes: number;
        export const Water_Acolyte_Wizard_Bottoms: number;
        export const Water_Acolyte_Wizard_Boots: number;
        export const Earth_Acolyte_Wizard_Hat: number;
        export const Earth_Acolyte_Wizard_Robes: number;
        export const Earth_Acolyte_Wizard_Bottoms: number;
        export const Earth_Acolyte_Wizard_Boots: number;
        export const Fire_Acolyte_Wizard_Hat: number;
        export const Fire_Acolyte_Wizard_Robes: number;
        export const Fire_Acolyte_Wizard_Bottoms: number;
        export const Fire_Acolyte_Wizard_Boots: number;
        export const Air_Adept_Wizard_Hat: number;
        export const Air_Adept_Wizard_Robes: number;
        export const Air_Adept_Wizard_Bottoms: number;
        export const Air_Adept_Wizard_Boots: number;
        export const Water_Adept_Wizard_Hat: number;
        export const Water_Adept_Wizard_Robes: number;
        export const Water_Adept_Wizard_Bottoms: number;
        export const Water_Adept_Wizard_Boots: number;
        export const Earth_Adept_Wizard_Hat: number;
        export const Earth_Adept_Wizard_Robes: number;
        export const Earth_Adept_Wizard_Bottoms: number;
        export const Earth_Adept_Wizard_Boots: number;
        export const Fire_Adept_Wizard_Hat: number;
        export const Fire_Adept_Wizard_Robes: number;
        export const Fire_Adept_Wizard_Bottoms: number;
        export const Fire_Adept_Wizard_Boots: number;
        export const Air_Expert_Wizard_Hat: number;
        export const Air_Expert_Wizard_Robes: number;
        export const Air_Expert_Wizard_Bottoms: number;
        export const Air_Expert_Wizard_Boots: number;
        export const Water_Expert_Wizard_Hat: number;
        export const Water_Expert_Wizard_Robes: number;
        export const Water_Expert_Wizard_Bottoms: number;
        export const Water_Expert_Wizard_Boots: number;
        export const Earth_Expert_Wizard_Hat: number;
        export const Earth_Expert_Wizard_Robes: number;
        export const Earth_Expert_Wizard_Bottoms: number;
        export const Earth_Expert_Wizard_Boots: number;
        export const Fire_Expert_Wizard_Hat: number;
        export const Fire_Expert_Wizard_Robes: number;
        export const Fire_Expert_Wizard_Bottoms: number;
        export const Fire_Expert_Wizard_Boots: number;
        export const Air_Imbued_Wand: number;
        export const Water_Imbued_Wand: number;
        export const Earth_Imbued_Wand: number;
        export const Fire_Imbued_Wand: number;
        const Red_Party_Hat_1: number;
        export { Red_Party_Hat_1 as Red_Party_Hat };
        export const Dragonfire_Shield: number;
        export const Circlet_of_Rhaelyx: number;
        export const Jewel_of_Rhaelyx: number;
        export const Charge_Stone_of_Rhaelyx: number;
        export const Crown_of_Rhaelyx: number;
        export const Enchanted_Cape: number;
        export const Enchanted_Shield: number;
        export const Mysterious_Stone: number;
        export const Event_Clue_1: number;
        export const Event_Clue_2: number;
        export const Event_Clue_3: number;
        export const Event_Clue_4: number;
        export const Cake_Base: number;
        export const Candle: number;
        export const Magical_Icing: number;
        export const Magical_Flavouring: number;
        export const Birthday_Cake: number;
        export const Birthday_Token: number;
        export const Purple_Party_Hat: number;
        export const Ancient_Ring_Of_Mastery: number;
        export const Cape_of_Completion: number;
        const Desert_Hat_1: number;
        export { Desert_Hat_1 as Desert_Hat };
        const Blazing_Lantern_1: number;
        export { Blazing_Lantern_1 as Blazing_Lantern };
        const Climbing_Boots_1: number;
        export { Climbing_Boots_1 as Climbing_Boots };
        export const Miolite_Helmet: number;
        export const Miolite_Boots: number;
        export const Miolite_Platelegs: number;
        export const Miolite_Platebody: number;
        export const Miolite_Shield: number;
        export const Miolite_Sceptre: number;
        export const Gloves_of_Silence: number;
        export const Shaman_Ring: number;
        export const Book_of_Occults: number;
        export const Elementalist_Gloves: number;
        export const Sand_Treaders: number;
        export const Desert_Wrappings: number;
        export const Desert_Sabre: number;
        export const Desert_Shortbow: number;
        export const Sandstorm_Ring: number;
        export const Darksteel_Dagger: number;
        export const Elder_Crown: number;
        export const Tormented_Ring: number;
        export const Sanguine_Blade: number;
        export const Recoil_Shield: number;
        export const Wasteful_Ring: number;
        export const Infernal_Claw: number;
        export const Tidal_Edge_Fragment: number;
        export const Tidal_Edge: number;
        export const Ocean_Song_Fragment: number;
        export const Ocean_Song: number;
        export const Shockwave_Fragment: number;
        export const Shockwave: number;
        export const Jadestone_Bolts: number;
        export const Paladin_Gloves: number;
        export const Priest_Hat: number;
        export const Almighty_Lute: number;
        export const Miolite_Chest: number;
        export const Infernal_Core: number;
        export const Infernal_Cape: number;
        export const Slayer_Helmet_Master: number;
        export const Slayer_Platebody_Master: number;
        export const Slayer_Cowl_Master: number;
        export const Slayer_Leather_Body_Master: number;
        export const Slayer_Wizard_Hat_Master: number;
        export const Slayer_Wizard_Robes_Master: number;
        const Green_Party_Hat_1: number;
        export { Green_Party_Hat_1 as Green_Party_Hat };
        export const Hunters_Ring: number;
        export const Futures_Prophecy: number;
        export const Unknown_Evil: number;
        export const New_Dawn: number;
        const Slayer_Upgrade_Kit_Strong_1: number;
        export { Slayer_Upgrade_Kit_Strong_1 as Slayer_Upgrade_Kit_Strong };
        const Slayer_Upgrade_Kit_Elite_1: number;
        export { Slayer_Upgrade_Kit_Elite_1 as Slayer_Upgrade_Kit_Elite };
        const Slayer_Upgrade_Kit_Master_1: number;
        export { Slayer_Upgrade_Kit_Master_1 as Slayer_Upgrade_Kit_Master };
        export const Santa_Hat: number;
        export const Christmas_Cracker: number;
        export const Friendship_Bracelet: number;
        export const Candy_Cane: number;
        export const Christmas_Coal: number;
        export const Christmas_Sweater: number;
        export const Christmas_Wreath: number;
        export const Yellow_Party_Hat: number;
        export const Mastery_Token_Agility: number;
        const Agility_Skillcape_1: number;
        export { Agility_Skillcape_1 as Agility_Skillcape };
        export const Performance_Enhancing_Potion_I: number;
        export const Performance_Enhancing_Potion_II: number;
        export const Performance_Enhancing_Potion_III: number;
        export const Performance_Enhancing_Potion_IV: number;
        export const Easter_Egg: number;
        export const Summoning_Shard_Red: number;
        export const Summoning_Shard_Green: number;
        export const Summoning_Shard_Blue: number;
        export const Summoning_Shard_Silver: number;
        export const Summoning_Shard_Gold: number;
        export const Summoning_Shard_Black: number;
        export const Summoning_Familiar_Golbin_Thief: number;
        export const Summoning_Familiar_Occultist: number;
        export const Summoning_Familiar_Wolf: number;
        export const Summoning_Familiar_Ent: number;
        export const Summoning_Familiar_Mole: number;
        export const Summoning_Familiar_Octopus: number;
        export const Summoning_Familiar_Minotaur: number;
        export const Summoning_Familiar_Centaur: number;
        export const Summoning_Familiar_Witch: number;
        export const Summoning_Familiar_Pig: number;
        export const Summoning_Familiar_Crow: number;
        export const Summoning_Familiar_Leprechaun: number;
        export const Summoning_Familiar_Cyclops: number;
        export const Summoning_Familiar_Yak: number;
        export const Summoning_Familiar_Unicorn: number;
        export const Summoning_Familiar_Dragon: number;
        export const Summoning_Familiar_Monkey: number;
        export const Summoning_Familiar_Salamander: number;
        export const Summoning_Familiar_Bear: number;
        export const Summoning_Familiar_Devil: number;
        export const Mastery_Token_Summoning: number;
        export const Summoning_Skillcape: number;
        export const Abnormal_Log: number;
        export const Red_Herring: number;
        export const Necromancer_Potion_I: number;
        export const Necromancer_Potion_II: number;
        export const Necromancer_Potion_III: number;
        export const Necromancer_Potion_IV: number;
        export const Necromancer_Hat: number;
        export const Necromancer_Robes: number;
        export const Necromancer_Bottoms: number;
        export const Necromancer_Boots: number;
        export const Cool_Glasses: number;
    }
}
declare const defaultBankSort: number[];
type general = number;
type skillUpgrades = number;
type slayer = number;
type skillcapes = number;
type materials = number;
declare function loadAgility(forceRestart?: boolean): void;
declare function createAgilityContainer(): void;
declare function startAgility(obstacle?: number, clear?: boolean, keepGoing?: boolean): void;
declare function getAgilityGPMultiplier(): number;
/**
 *
 * @param {ObstacleID} obstacleID
 * @param {boolean} [offline=false]
 */
declare function provideAgilityCompletionBonuses(
    obstacleID: ObstacleID,
    offline?: boolean
): {
    gp: number;
    items: ItemQuantity2[];
};
declare function updateAgilityIntervals(): void;
/**
 *
 * @param {number} obstacleID
 * @param {AgilityPillar[]} agilityArray
 */
declare function displayPassiveBonuses(obstacleID: number, agilityArray?: AgilityPillar[]): string;
declare function createAgilityProgress(): void;
/**
 *
 * @param {ObstacleID} id
 */
declare function createObstacleBuilderElement(id: ObstacleID): string;
declare function createPassivePillarBuilderElement(): string;
/**
 *
 * @param {ObstacleID} obstacleID
 */
declare function updateChosenAgilityObstaclePassiveBonuses(obstacleID: ObstacleID): void;
/**
 *
 * @param {ObstacleID} obstacleID
 * @param {boolean} [obstacleActive=true]
 */
declare function createChosenAgilityObstacleElement(obstacleID: ObstacleID, obstacleActive?: boolean): string;
/**
 *
 * @param {ObstacleID} obstacleID
 */
declare function createSelectAgilityObstacleElement(obstacleID: ObstacleID): string;
/**
 *
 * @param {PillarID} pillarID
 */
declare function createSelectPassivePillarElement(pillarID: PillarID): string;
/**
 *
 * @param {PillarID} pillarID
 * @param {boolean} isActive
 */
declare function createChosenPassivePillarElement(pillarID: PillarID, isActive: boolean): string;
/**
 *
 * @param {ObstacleID} obstacleID
 * @param {boolean} [showMasteryLevelSeparate=false]
 * @param {boolean} [showCostReduction=false]
 */
declare function createSelectAgilityObstacleDescription(
    obstacleID: ObstacleID,
    showMasteryLevelSeparate?: boolean,
    showCostReduction?: boolean
): string;
/**
 *
 * @param {ObstacleID} obstacleID
 * @returns {string}
 */
declare function getAgilityObstacleCostReductionElement(obstacleID: ObstacleID): string;
/**
 *
 * @param {ObstacleID} obstacleID
 */
declare function getSelectAgilityObstacleBenefits(obstacleID: ObstacleID): string;
/**
 *
 * @param {ObstacleID} obstacleID
 * @returns {string}
 */
declare function getAgilityObstacleGPBonus(obstacleID: ObstacleID): string;
/**
 *
 * @param {ObstacleID} obstacleID
 */
declare function updateAgilityObstacleGPBonus(obstacleID: ObstacleID): void;
/**
 *
 * @param {ObstacleID} obstacleID
 * @returns {string}
 */
declare function getAgilityObstacleXPBonus(obstacleID: ObstacleID): string;
/**
 *
 * @param {ObstacleID} obstacleID
 */
declare function updateAgilityObstacleXPBonus(obstacleID: ObstacleID): void;
/**
 *
 * @param {ObstacleID} obstacleID
 * @returns
 */
declare function getAgilityObstacleSlayerCoinsBonus(obstacleID: ObstacleID): any;
/**
 *
 * @param {ObstacleID} obstacleID
 */
declare function updateAgilityObstacleSlayerCoinsBonus(obstacleID: ObstacleID): void;
declare function updateAllAgilityBonusesPerObstacle(): void;
/**
 *
 * @param {ObstacleID} obstacleID
 */
declare function getSelectAgilityObstacleRequirements(obstacleID: ObstacleID): string;
/**
 *
 * @param {ObstacleID} obstacleID
 * @returns {number}
 */
declare function getAgilityObstacleCostReduction(obstacleID: ObstacleID): number;
/**
 *
 * @param {ObstacleID} obstacleID
 * @returns {number}
 */
declare function getAgilityObstacleItemCostReduction(obstacleID: ObstacleID): number;
/**
 *
 * @param {number} obstacleID
 * @param {AgilityPillar[]} agilityArray
 * @param {boolean} [reduceCosts=true]
 */
declare function getSelectAgilityObstacleCost(
    obstacleID: number,
    agilityArray?: AgilityPillar[],
    reduceCosts?: boolean
): string;
/**
 * onClick callback function
 * @param {ObstacleCategories} category
 */
declare function displaySelectAgilityObstacle(category: ObstacleCategories): void;
declare function displaySelectPassivePillar(): void;
declare function destroyAgilityObstacle(category: any, confirmed?: boolean): void;
declare function destroyAgilityPassivePillar(confirmed?: boolean): void;
declare function buildAgilityObstacle(obstacleID: any, confirmed?: boolean): void;
declare function buildAgilityPassivePillar(pillarID: any, confirmed?: boolean): void;
declare function applyCostsToPlayer(definedCosts: any, obstacleID: any, qty?: number, reduceCosts?: boolean): void;
declare function canIAffordThis(
    definedCosts: any,
    definedRequirements: any,
    obstacleID: any,
    reduceCosts?: boolean
): boolean;
declare function checkUnlockRequirements(definedRequirements: any): boolean;
declare function updateAgilityBreakdown(): void;
declare function showAllAgilityPassives(): void;
declare function getAgilityModifierValue(obstacleID: any, key: any, value: any): any;
/** @type {AgilityObstacle[]} */
declare const agilityObstacles: AgilityObstacle[];
/** @type {AgilityPillar[]} */
declare const agilityPassivePillars: AgilityPillar[];
/** Save game variable */
declare var agilityPassivePillarActive: number;
/**
 * Save game variable
 * @type {ObstacleID[]} */
declare var chosenAgilityObstacles: ObstacleID[];
/**
 * Save game variable
 * @type {number[]} */
declare var agilityObstacleBuildCount: number[];
declare var isAgility: boolean;
declare var currentObstacle: number;
/** @type {TimeoutID} */
declare var agilityTimer: TimeoutID;
declare function loadItemsAlreadyFound(): void;
/**
 *
 * @param {ItemID} itemID
 */
declare function checkBrandNewItem(itemID: ItemID): void;
/**
 *
 * @param {ItemID} itemID
 */
declare function updateBrandNewItem(itemID: ItemID): void;
/**
 *
 * @param {string} search
 */
declare function updateBankSearch(search: string): void;
/**
 * Initial loading of the bank. Used to refresh entire bank as well.
 */
declare function loadBank(): void;
/**
 * onClick callback function
 */
declare function clearBankSearch(): void;
declare function updateBank(): void;
declare function getBankValue(): void;
/**
 * Update an existing item in the bank. It only runs if the condition that executes this finds the same item in the bank already.
 * @param {BankID|false} bankID
 * @param {ItemID} itemID
 * @param {number} quantity
 * @param {boolean} [ignoreAdd=false]
 * @param {boolean} [ignoreUpdate=false]
 */
declare function updateItemInBank(
    bankID: BankID | false,
    itemID: ItemID,
    quantity: number,
    ignoreAdd?: boolean,
    ignoreUpdate?: boolean
): void;
/**
 * onClick callback function
 */
declare function sortBank(): void;
/**
 * Remove an item from the bank when qty = 0
 * Assumes itemID exists in bank
 * @param {ItemID} itemID
 */
declare function removeItemFromBank(itemID: ItemID): void;
/**
 *
 * @param {ItemID} itemID
 */
declare function checkBankForItem(itemID: ItemID): boolean;
/**
 *
 * @param {ItemID} itemID
 */
declare function getBankId(itemID: ItemID): number;
/**
 *
 * @param {ItemID} itemID
 * @returns {number}
 */
declare function getBankQty(itemID: ItemID): number;
/**
 *
 * @param {number} currentBankSpace
 */
declare function checkAddItemToBank(currentBankSpace: number): boolean;
/**
 * Add a new item to the bank.
 * @param {ItemID} itemID
 * @param {number} quantity
 * @param {boolean} [found=true]
 * @param {boolean} [showNotification=true]
 * @param {boolean} [ignoreBankSpace=false]
 */
declare function addItemToBank(
    itemID: ItemID,
    quantity: number,
    found?: boolean,
    showNotification?: boolean,
    ignoreBankSpace?: boolean
): boolean;
declare function getMaxBankSpace(): number;
/**
 * creation of a bank item
 * @param {ItemID} bankItemID
 * @param {BankID} [bankButtonID=0]
 * @returns {[number,string]}
 */
declare function createBankItem(bankItemID: ItemID, bankButtonID?: BankID): [number, string];
/**
 *
 * @param {ItemID} bankItemID
 */
declare function createBankItemEvents(bankItemID: ItemID): void;
/**
 *
 * @param {ItemID} itemID
 * @param {boolean} [showStats=false]
 * @returns {string}
 */
declare function createItemInformationTooltip(itemID: ItemID, showStats?: boolean): string;
/**
 *
 * @param {ItemID} itemID
 * @returns {string}
 */
declare function getItemSpecialAttackInformation(itemID: ItemID): string;
/**
 *
 * @param {ItemID} itemID
 */
declare function getItemSalePrice(itemID: ItemID): number;
/**
 *
 * @param {ItemID} itemID
 * @param {boolean} [toggleSidebar=true]
 */
declare function selectBankItem(itemID: ItemID, toggleSidebar?: boolean): void;
declare function updateBankItemSettings(): void;
declare function getTotalGPSaleMode(): number;
declare function getTotalCountSaleMode(): number;
declare function updateSelectedBankItemQty(): void;
/**
 * onClick callback function
 * @param {boolean} [confirmed=false]
 */
declare function confirmSellModeSelection(confirmed?: boolean): void;
/**
 * onClick callback function
 */
declare function confirmMoveModeSelection(): void;
/**
 * onClick callback function
 */
declare function sellItem(): void;
/**
 *
 * @param {ItemID} itemID
 * @param {number} qty
 * @param {number} [saleModifier=1]
 */
declare function processItemSale(itemID: ItemID, qty: number, saleModifier?: number): void;
declare function createVirtualBank(): void;
/**
 *
 * @param {ItemID} itemID
 * @param {number} qty
 */
declare function addItemToVirtualBank(itemID: ItemID, qty: number): boolean;
/**
 * onClick callback function
 */
declare function openBankItem(): void;
/**
 * onClick callback function
 * @param {ItemID} [itemID=-1]
 */
declare function viewItemContents(itemID?: ItemID): void;
/**
 * onClick callback function
 * @param {MonsterID} monsterID
 */
declare function viewMonsterDrops(monsterID: MonsterID): void;
/**
 * onClick callback function
 */
declare function upgradeItem(): void;
/**
 * onClick callback function
 * @param {ItemID} itemID
 * @param {ItemID} upgradeItemID
 */
declare function confirmUpgradeItemAll(itemID: ItemID, upgradeItemID: ItemID): void;
/**
 *
 * @param {ItemID} itemID
 * @param {ItemID} upgradeItemID
 * @param {number} [qty=1]
 */
declare function confirmUpgradeItem(itemID: ItemID, upgradeItemID: ItemID, qty?: number): void;
/**
 * onClick callback function
 * @param {0|1} option
 */
declare function updateSellQty(option: 0 | 1): void;
declare function claimToken(forceItem?: number, forceQty?: number, updateSpendXP?: boolean): void;
/**
 * onClick callback function
 */
declare function buryItem(): void;
/**
 * onClick callback function
 */
declare function findAFriend(): void;
declare function readItem(itemID: any): void;
declare function claimBankToken(): void;
declare function lockItem(): void;
declare function updateLockedItemIcon(): void;
declare function useEight(): void;
declare function toggleBankBorders(): void;
declare function getItemQty(itemID: any, format?: boolean): any;
declare function updateSellRangeSlider(itemID: any, qty?: any): void;
declare function checkSaleButtons(from: any, max: any): void;
declare function updateEquipItemContainer(itemID: any, ignoreSetting?: boolean): void;
declare function createEquipToSetButtons(): string;
declare function createReplaceItemHTML(item: any, slot: any): string;
/**
 *
 * @param {string} selector1
 * @param {string} selector2
 * @param {number} itemID
 * @param {(data: *) => void} onStart
 * @param {(data: *) => void} onChange
 */
declare function initializeBankRangeSliders(
    selector1: string,
    selector2: string,
    itemID: number,
    onStart: (data: any) => void,
    onChange: (data: any) => void
): void;
declare function updateEquipItemQuantitySlider(itemID: any): void;
declare function updateEquipFoodSlider(itemID: any): void;
declare function updateOpenItemSlider(itemID: any): void;
declare function updateBuryItemSlider(itemID: any): void;
declare function updateClaimTokenSlider(itemID: any): void;
declare function deselectBankItem(): void;
declare function changeEquipToSet(set: any, updateEquip?: boolean): void;
declare function showBankTab(tab: any): void;
declare function setTabToDrag(tab: any): void;
declare function unsetTabToDrag(tab: any): void;
declare function dropItemNewTab(tab: any): void;
declare function sortBankByTabs(): void;
declare function getNewBankIndex(index: any): any;
declare function getIndexOfItem(currentIndex: any, tab: any): any;
declare function updateBankTabImage(tab: any, returnSrc?: boolean): string;
declare function updateBankTabImages(): void;
declare function openDefaultBankTabDropdown(): void;
declare function createSortableInstances(): void;
declare function fireOnEnd(newIndex: any, oldIndex: any, tab: any): void;
declare function mapOrder(array: any, order: any, key: any): any;
declare function toggleSellItemMode(): void;
declare function updateSellItemMode(): void;
declare function addItemToItemSaleArray(item: any): void;
declare function addItemToItemMoveArray(item: any): void;
declare function updateItemToSellBorder(item: any): void;
declare function updateItemToSellSummary(): void;
declare function updateItemToMoveSummary(): void;
declare function updateItemToMoveBorder(item: any): void;
declare function removeSellItemBorders(): void;
declare function removeMoveItemBorders(): void;
declare function toggleMoveItemMode(): void;
declare function updateMoveItemMode(): void;
declare function setDefaultBankSorting(sort: any): void;
declare function fixBankArray(): void;
declare function createBankStackValue(): void;
declare function updateBankStackValue(bankID: any): void;
declare function assignDefaultItemTab(itemID: any): number;
declare function setDefaultItemTab(tab: any, itemID: any): void;
declare function setMoveItemTab(tab: any): void;
declare function logConsole(message: any): void;
/** @type {BankItem[]} */
declare var bank: BankItem[];
declare const baseBankMax: 12;
declare const maxTabs: 10;
/** @deprecated Old Save Variable */
declare var bankMax: number;
/** Save game variable */
declare var myBankVersion: number;
/** Save game variable */
declare var sellQty: number;
/** @type {ItemID|null} */
declare var selectedBankItem: ItemID | null;
/** @type {null|TimeoutID} */
declare var addItemUpdateTimer: null | TimeoutID;
/** @type {number[]} */
declare var nextItem: number[];
/** @type {ItemQuantity[]} */
declare var vBank: ItemQuantity[];
/** @type {BankID|null} */
declare var itemInViewPos: BankID | null;
/**
 * Save game variable
 * @type {ItemID[]} */
declare var lockedItems: ItemID[];
declare var allButOne: boolean;
/** @type {ItemID|null} */
declare var bankItemSelected: ItemID | null;
declare var openItemQty: number;
declare var claimTokenQty: number;
declare var buryItemQty: number;
/** @type {EquipSetID} */
declare var equipToSet: EquipSetID;
/** @type {BankTabID} */
declare var selectedBankTab: BankTabID;
declare var newTabSelected: boolean;
/** @type {null|BankTabID} */
declare var newTabToDrag: null | BankTabID;
declare var itemBeingDragged: number;
declare let sellItemMode: boolean;
/** @type {ItemID[]} */
declare let itemsToSell: ItemID[];
declare let moveItemMode: boolean;
/** @type {ItemID[]} */
declare let itemsToMove: ItemID[];
/** @type {BankTabID} */
declare let moveItemModeTab: BankTabID;
declare var equipFoodQty: number;
/** @type {ItemID[]} */
declare var itemsAlreadyFound: ItemID[];
/** @type {null|TimeoutID} */
declare let updateSalePriceTimer: null | TimeoutID;
/** @type {BankCache} */
declare var bankCache: BankCache;
/** @type {null|TimeoutID} */
declare var getBankValueTimer: null | TimeoutID;
declare function updateBankSearchArray(): void;
/** @type {BankSearch[]} */
declare var bankSearch: BankSearch[];
declare function updateMbucks(): void;
declare function begParentsForMBucksBecauseImBrokeAndCantAffordToBuyItInTheP2WShopEvenThoughIShouldProbablyMakeDragonJavsForMoneyOhWaitLol(): void;
declare function viewItemsAcquired(): void;
/**
 *
 * @param {number} qty
 */
declare function buyMbucks(qty: number): void;
declare var mbucks: number;
declare var caseInventory: any[];
declare var totalMbucksSpent: number;
/**
 * onClick callback function
 * @param {boolean} [confirmed=false]
 */
declare function loadCloudSave(confirmed?: boolean): void;
declare function startCloudSync(init?: boolean): void;
declare function getCloudCharacters(): void;
declare function checkConnectionToCloud(forceLoad?: boolean, accessCheck?: boolean): void;
declare function deleteCloudSave(): void;
declare function forceSync(closeAfter?: boolean, ignorePlayFab?: boolean): void;
declare function forceSyncCooldown(): void;
declare function checkGameVersion(): void;
declare function checkPatreon(): void;
declare function disconnectPatreon(): void;
declare function loadCloudOptions(isCloud: any): void;
declare function checkTestAccess(): void;
declare function checkTestAccessInit(forceLoad?: boolean, accessCheck?: boolean): void;
declare function checkTestAccessPatreon(forceLoad?: boolean, accessCheck?: boolean): void;
declare function confirmTestAccess(): void;
declare function killPage(): void;
declare function connectToPlayfabOffline(): void;
declare function generatePlayfabOfflineID(): void;
declare function playFabLoginWithCustomID(playFabID: any, offlineID?: boolean): void;
declare function getPlayFabSave(): void;
declare function loadCallback(result: any, error: any): void;
declare function playFabSaveData(forceSave?: boolean, closeAfterSave?: boolean): void;
declare function saveCallback(result: any, error: any, forceSave?: boolean): void;
declare function showPlayFabSaveSuccessfulNotification(): void;
declare function createPlayFabSaves(char: any): string;
declare function enableCloudCharacterButton(): void;
declare function deletePlayFabSave(characterID?: number): void;
declare function sendPlayFabEvent(eventName: any, args: any): void;
declare function sendPlayFabEventCallback(result: any, error: any): void;
declare function queuePlayFabEvents(eventName: any, args: any): void;
declare function submitQueuedPlayFabEvents(): void;
declare function fetchLatestTitleNews(): void;
declare function displayLatestTitleNews(result: any, error: any): void;
declare function createTitleNewsElement(timestamp: any, title: any, body: any, newsId: any): string;
declare function readPlayFabNews(): void;
declare function loginToMelvorCloud(): void;
declare function registerToMelvorCloud(): void;
declare function logoutMelvorCloud(): void;
declare function forgotPasswordMelvorCloud(): void;
declare function disableLoginForm(): void;
declare function enableLoginForm(): void;
declare function disableRegisterForm(): void;
declare function enableRegisterForm(): void;
declare function disableForgotForm(): void;
declare function enableForgotForm(): void;
declare function disableChangeEmailForm(): void;
declare function enableChangeEmailForm(): void;
declare function disableChangePasswordForm(): void;
declare function enableChangePasswordForm(): void;
declare function updateEmailMelvorCloud(): void;
declare function updatePasswordMelvorCloud(): void;
declare function cloudSaveAndExit(): void;
declare function updateLastCloudSaveTimestamp(): void;
/** @type {TimeoutID|null} */
declare var cloudSaveTimer: TimeoutID | null;
/** @type {SaveString|null} */
declare var cloudSave: SaveString | null;
declare var forceSaveCooldown: boolean;
/** @type {TimeoutID|null} */
declare var forceSyncCooldownTimer: TimeoutID | null;
declare var connectedToCloud: boolean;
declare var connectedToPlayFab: boolean;
declare var connectedToPlayFabOffline: boolean;
/** @type {string[]} */
declare var storedCloudSaves: string[];
/** @type {(null|SaveString)[]} */
declare var playFabSaves: (null | SaveString)[];
declare var playFabLoginTimestamp: number;
declare let forceSave: boolean;
/** @type {Date|number} */
declare var lastSaveTimestamp: Date | number;
declare var lastLoginTimestamp: number;
declare var saveAndClose: boolean;
/** @type {TimeoutID|null} */
declare var forceSyncSpamPrevention: TimeoutID | null;
declare var connectingToCloud: boolean;
declare function playFabLoginCallback(result: any, error: any): void;
declare function playFabLoginCallbackOffline(result: any, error: any): void;
/**
 *
 * @param {0|1} qty Unused
 * @param {boolean} [ignore=true]
 */
declare function startCooking(qty: 0 | 1, ignore?: boolean): void;
/**
 *
 * @param {number} cookingID
 * @param {number} cookingInterval
 * @returns {number}
 */
declare function getPrimaryBurnChance(cookingID: number, cookingInterval: number): number;
declare function getSecondaryBurnChance(): number;
declare function updateCookingFire(): void;
declare function updateAvailableFood(): void;
/**
 *
 * @param {ItemID} id
 */
declare function selectFood(id: ItemID): void;
/**
 *
 * @param {ItemID} food
 */
declare function checkFoodExists(food: ItemID): boolean;
declare function loadCooking(): void;
/** @type {CookingItem[]} */
declare var cookingItems: CookingItem[];
declare var isCooking: boolean;
/** @type {TimeoutID|null} */
declare var currentlyCooking: TimeoutID | null;
/** @type {ItemID|null} */
declare var selectedFood: ItemID | null;
/** @type {BankItem} */
declare var foodCache: BankItem;
/** @type {BankID} */
declare var foodCacheID: BankID;
declare function startCrafting(clicked?: boolean): void;
/**
 *
 * @param {number} craftingID
 * @param {boolean} [update=false]
 */
declare function selectCraft(craftingID: number, update?: boolean): void;
declare function loadCrafting(): void;
declare function updateCrafting(): void;
declare function getCraftingRecipeQty(itemID: any, recipeIndex: any, useCharge?: boolean): number;
declare function updateCraftQty(itemID: any): void;
declare function craftCategory(cat: any): void;
declare function checkCraftingReq(itemID: any): boolean;
/** @type {CraftingItem[]} */
declare var craftingItems: CraftingItem[];
/** @type {number|null} */
declare var currentCraft: number | null;
declare var isCrafting: boolean;
/** @type {TimeoutID|null} */
declare var craftingTimeout: TimeoutID | null;
/** @type {number|numberMultiplier} */
declare var selectedCraft: number | number;
/** @type {QtyReqCheck[]} */
declare var craftReqCheck: QtyReqCheck[];
declare const er: 'i';
declare var craftInterval: number;
/** @type {ItemID[]} */
declare var craftingJewelleryIDs: ItemID[];
/**
 *
 * @param {0|1|2|3} eventID
 * @param {null|MonsterID|string} [enemyThatKilledYou=null]
 */
declare function sendDiscordEvent(eventID: 0 | 1 | 2 | 3, enemyThatKilledYou?: null | MonsterID | string): void;
/** @type {PlayFabModule.ApiCallback<PlayFabClientModels.ExecuteCloudScriptResult>} */
declare function OnCloudDiscordEvent(
    result: PlayFabModule.SuccessContainer<PlayFabClientModels.ExecuteCloudScriptResult>
): void;
/** I'm pretty sure this function is effectively useless and only feeds through to the callback as result.customData */
declare function OnErrorShared(error: any): void;
/**
 *
 * @param {FarmingAreaID} areaID
 */
declare function loadFarmingArea(areaID: FarmingAreaID): void;
/**
 * onClick callback function
 * @param {FarmingAreaID} areaID
 * @param {number} patchID
 */
declare function unlockPlot(areaID: FarmingAreaID, patchID: number): void;
/**
 *
 * @param {FarmingAreaID} areaID
 */
declare function updateTimeRemaining(areaID: FarmingAreaID): void;
declare function showFarmingAreas(): void;
declare function loadSeeds(): void;
/**
 * onClick callback function
 * @param {FarmingAreaID} areaID
 * @param {number} patchID
 * @param {boolean} [plantAll=false]
 */
declare function showSeeds(areaID: FarmingAreaID, patchID: number, plantAll?: boolean): void;
/**
 * onClick callback function
 * @param {ItemID} itemID
 * @param {boolean} [plantAll=false]
 */
declare function selectSeed(itemID: ItemID, plantAll?: boolean): void;
/**
 * onClick callback function
 * @param {boolean} [plantAll=false]
 */
declare function plantSeed(plantAll?: boolean): void;
declare function removeSeed(areaID: any, patchID: any): void;
/**
 * onClick callback function
 * @param {FarmingAreaID} areaID
 * @param {number} patchID
 */
declare function harvestSeed(areaID: FarmingAreaID, patchID: number, all?: boolean): void;
declare function startSeedTimer(areaID: any, patchID: any, initialise?: boolean): void;
declare function getPlantInterval(areaID: any, patchID: any): number;
declare function getSeedGrowTime(seedID: any): number;
declare function setPlantInterval(areaID: any, patchID: any): number;
declare function notifyPlayerHarvestReady(): void;
declare function growCrops(areaID: any, patchID: any): void;
declare function addCompost(areaID: any, patchID: any, qty?: number): void;
declare function addGloop(areaID: any, patchID: any): void;
declare function resetFarmingPatch(areaID: any, patchID: any): void;
declare function checkReadyToHarvest(): void;
declare function showHarvestReady(ready?: boolean): void;
declare function harvestAll(): void;
declare function compostAll(): void;
declare function gloopAll(): void;
declare function updateCompostQty(): void;
/**
 * Save game variable
 * @type {FarmingArea[]} */
declare var newFarmingAreas: FarmingArea[];
/** @type {Seed[]} */
declare var allotmentSeeds: Seed[];
/** @type {Seed[]} */
declare var herbSeeds: Seed[];
/** @type {Seed[]} */
declare var treeSeeds: Seed[];
/** @type {ItemID} */
declare var selectedSeed: ItemID;
/** @type {[FarmingAreaID,number]|[]} */
declare var selectedPatch: [FarmingAreaID, number] | [];
/** @type {FarmingAreaID|null} */
declare var currentFarmingArea: FarmingAreaID | null;
/** @type {TimeoutID|null} */
declare var farmingGlower: TimeoutID | null;
declare const harvestAllCost: 2000;
declare const compostAllCost: 2000;
declare const plantAllCost: 5000;
/** @type {TimeoutID|null} */
declare var harvestReadyTimer: TimeoutID | null;
/**
 * @deprecated Unused variable?
 * @type {null}
 */
declare var sendFarmingNotificationTimer: null;
declare function burnLog(ignore?: boolean): void;
declare function getCoalChance(): number;
declare function lightBonfire(): void;
/**
 *
 * @param {FiremakingID} log FiremakingID of log
 */
declare function checkLogExists(log: FiremakingID): boolean;
declare function updateAvailableLogs(skill?: number): void;
/**
 *
 * @param {FiremakingID} id
 * @param {SkillID} skill
 */
declare function selectLog(id: FiremakingID, skill?: SkillID): void;
declare const logsData: {
    type: string;
    level: number;
    interval: number;
    bonfireInterval: number;
    bonfireBonus: number;
    xp: number;
}[];
/** @type {FiremakingID|null} */
declare var selectedLog: FiremakingID | null;
declare var isBurning: boolean;
/** @type {TimeoutID|null} */
declare var currentlyBurning: TimeoutID | null;
/** @type {TimeoutID|null} */
declare var currentBonfireHandler: TimeoutID | null;
declare var bonfireBonus: number;
/** @type {BankItem} */
declare var logCache: BankItem;
/** @type {BankID} */
declare var logCacheID: BankID;
/**
 *
 * @param {FishingAreaID} areaID
 * @param {FishID} fishID
 * @param {boolean} [clicked=false]
 */
declare function startFishing(areaID: FishingAreaID, fishID: FishID, clicked?: boolean): void;
declare function loadFishing(): void;
declare function setupFishingAreas(): void;
/**
 *
 * @param {FishingAreaID} areaID
 * @param {FishID} fishID
 */
declare function selectFish(areaID: FishingAreaID, fishID: FishID): void;
/**
 *
 * @param {FishingAreaID} areaID
 * @param {FishID} fishID
 */
declare function updateFishingAreaWeights(areaID: FishingAreaID, fishID: FishID): void;
declare function updateAvailableFish(): void;
/**
 *
 * @param {FishingAreaID} areaID
 * @param {FishID} fishID
 */
declare function updateFishingMastery(areaID: FishingAreaID, fishID: FishID): void;
declare function updateFishing(): void;
declare function secretAreaCheck(): void;
declare function barbarianAreaCheck(): void;
declare const fishingAreas: (
    | {
          name: string;
          fishChance: number;
          junkChance: number;
          specialChance: number;
          fish: number[];
          description?: undefined;
      }
    | {
          name: string;
          description: string;
          fishChance: number;
          junkChance: number;
          specialChance: number;
          fish: number[];
      }
)[];
/** @type {FishingItem[]} */
declare var fishingItems: FishingItem[];
/** @type {ItemID[]} */
declare var junkItems: ItemID[];
/** @type {LootTable} */
declare var specialItems: LootTable;
declare var isFishing: boolean;
/** @type {FishingAreaID|null} */
declare var currentFishingArea: FishingAreaID | null;
/** @type {(null|FishingID)[]} */
declare var selectedFish: (null | FishingID)[];
/** @type {TimeoutID|null} */
declare var fishingTimeout: TimeoutID | null;
/** Save game variable */
declare var secretAreaUnlocked: boolean;
/**
 *
 * @param {boolean} [clicked=false]
 */
declare function startFletching(clicked?: boolean): void;
/**
 *
 * @param {FletchingID} fletchingID
 * @param {FletchLog} [log=0]
 * @param {boolean} [update=false]
 */
declare function selectFletch(fletchingID: FletchingID, log?: FletchLog, update?: boolean): void;
declare function loadFletching(): void;
declare function updateFletching(): void;
declare function getFletchingRecipeQty(itemID: any, recipeIndex: any): number;
declare function updateQty(itemID: any): void;
declare function fletchCategory(cat: any): void;
declare function checkFletchingReq(itemID: any, fletchLog?: number, offline?: boolean): boolean;
/** @type {FletchingItem[]} */
declare var fletchingItems: FletchingItem[];
/** @type {FletchingID|null} */
declare var currentFletch: FletchingID | null;
declare var isFletching: boolean;
/** @type {TimeoutID|null} */
declare var fletchingTimeout: TimeoutID | null;
/** @type {QtyReqCheck[]} */
declare var fletchReqCheck: QtyReqCheck[];
/** @type {FletchingID|null} */
declare var selectedFletch: FletchingID | null;
/** @type {FletchLog} */
declare var selectedFletchLog: FletchLog;
declare var fletchInterval: number;
declare function updateRaidCoins(qty?: number): void;
declare function loadGolbinRaidHistory(): void;
/**
 *
 * @param {number} historyID
 * @returns {string}
 */
declare function getGolbinRaidHistory(historyID: number): string;
declare var isGolbinRaid: boolean;
/**
 * Save game variable
 * @type {RaidHistory[]} */
declare var golbinRaidHistory: RaidHistory[];
/** Save game variable */
declare var raidCoins: number;
/** Save game variable */
declare var golbinRaidStats: number[];
declare const bannedGolbinRaidItems: number[];
declare const golbinNames: string[];
declare const golbinTraits: string[];
declare function startHerblore(clicked?: boolean): void;
declare function loadHerblore(update?: boolean): void;
declare function selectHerblore(herbloreID: any, update?: boolean): void;
declare function getHerbloreRecipeQty(itemID: any, recipeIndex: any): number;
declare function updateHerbloreQty(itemID: any): void;
declare function getHerbloreTier(id: any): number;
declare function checkHerbloreReq(itemID: any): boolean;
declare function herbloreCategory(cat: any): void;
declare function loadPotions(): void;
declare function usePotion(itemID: any, isOffline?: boolean): void;
declare function updateHerbloreBonuses(itemID: any, qty?: number, use?: boolean, isOffline?: boolean): void;
declare function getPotionCharges(itemID: any): number;
declare function updatePotionHeader(): void;
/** @type {HerbloreItem[]} */
declare const herbloreItemData: HerbloreItem[];
/** @type {typeof herbloreItemData} */
declare var herbloreItems: typeof herbloreItemData;
declare var isHerblore: boolean;
/** @type {TimeoutID|null} */
declare var herbloreTimeout: TimeoutID | null;
/** @type {QtyReqCheck[]} */
declare var herbloreReqCheck: QtyReqCheck[];
/** @type {HerbloreItemID|null} */
declare var selectedHerblore: HerbloreItemID | null;
/** @type {HerbloreItemID|null} */
declare var currentHerblore: HerbloreItemID | null;
declare var herbloreInterval: number;
declare const masteryTiers: number[];
/** Contains the pageIDs of pages that have potions */
declare const potionPages: number[];
/** @type {NumberDictionary<HerbloreBonus>} */
declare var herbloreBonuses: NumberDictionary<HerbloreBonus>;
/** @type {GenericItem[]} */
declare const items: GenericItem[];
declare function updateWindow(): void;
declare function updateGP(value?: number, disableDOMChanges?: boolean): void;
declare function idleChecker(currentSkill: any): boolean;
declare function updateSkillVisuals(skill: any): void;
/** Specific window updates for each skill (So we're not updating the entire game every time) */
declare function updateSkillWindow(skill: any): void;
/** Update the progress bar to the correct value */
declare function updateLevelProgress(skill: any): void;
/** Level up the skill when required xp is reached */
declare function levelUp(skill: any, offline?: boolean, render?: boolean): void;
declare function convertGP(currentGP: any): any;
declare function formatNumber(number: any): any;
declare function numberWithCommas(x: any): any;
declare function unlockSkill(skill: any): void;
/**
 *
 * @param {number|string} x
 * @returns {string}
 */
declare function getPriceToUnlockSkill(): string;
declare function changePage(
    page: any,
    gameLoading?: boolean,
    toggleSidebar?: boolean,
    showRaidShop?: boolean,
    skillID?: number
): void;
declare function setToUppercase(string: any): any;
declare function setToLowercase(string: any): any;
declare function updateTooltips(): void;
declare function itemNotify(itemID: any, qty: any): void;
declare function processItemNotify(itemID: any, qty: any): void;
declare function gpNotify(qty: any): void;
declare function stunNotify(damage: any): void;
declare function bankFullNotify(): void;
declare function levelUpNotify(skill: any): void;
declare function createNewMilestoneModal(skill: any, oldLevel: any, newLevel: any): string;
declare function showNewMilstones(skill: any): void;
declare function notifyPlayer(skill: any, message: any, type?: string): void;
declare function notifyGloves(skill: any): void;
declare function notifySlayer(qty: any, type?: string): void;
declare function updateMilestoneTab(skill: any): void;
declare function selectFromDropTable(itemID: any): number;
declare function updateGloves(gloves: any, skill: any): void;
declare function addXP(skill: any, xp: any, render?: boolean, dropRares?: boolean, offline?: boolean): boolean;
declare function getSkillXPToAdd(skill: any, xp: any): number;
declare function getMasteryToken(skill: any, offline?: boolean): number;
declare function gloveCheck(): void;
declare function toggleMenu(menu: any): void;
declare function rollForGeneralRareItems(
    skill: any,
    totalBaseActions: any,
    petID: any,
    interval: any,
    tokenID: any,
    levelReq: any
): {
    itemsGained: any;
    actions: number;
}[];
declare function updateOffline(continueAction?: boolean): void;
declare function clearOffline(save?: boolean): void;
declare function offlineCatchup(): void;
declare function toggleCombatSkillMenu(): void;
declare function dropRingHalfA(levelReq: any): void;
declare function activateTutorialTip(tipID: any): void;
declare function updateSaveFileChanges(): void;
/**
 *
 * @param {number} levelReq
 */
declare function pauseOfflineAction(skill: any): void;
declare function initMinibar(): void;
declare function updateMinibar(pageName: any): void;
declare function updateMinibarEquippedIcon(skillID: any): void;
/**
 *
 * @param {SkillID} skillID
 */
declare function toggleSkillMinibar(): void;
declare function quickEquipSkillcape(skill: any): void;
declare function quickEquipItem(item: any, skill: any): void;
declare function setGamemode(gamemode: any): void;
declare function loadCharacterSelection(returnToGame?: boolean): void;
/**
 *
 * @param {boolean} [cloudSave=false]
 * @param {number} gamemode
 * @param {string} timestamp
 * @param {number} characterID
 * @param {NewSaveGame} saveGame
 * @returns {string}
 */
/**
 *
 * @param {boolean} cloudSave
 * @param {CharacterID} characterID
 * @param {NewSaveGame} saveGame
 * @param {boolean} [oldFormat=false]
 * @returns {string}
 */
declare function createCharacterSelectionBox(
    cloudSave: boolean,
    characterID: CharacterID,
    saveGame: NewSaveGame,
    oldFormat?: boolean
): string;
declare function showDiscontinuedModal(): void;
declare function createNewCharacterElement(characterID?: number): string;
declare function compareSaveTimestamps(): {
    local: number;
    cloud: number;
}[];
declare function displaySaveTimestampComparison(): void;
declare function processLocalCharacters(): void;
declare function processCloudCharacters(charID: any, saveString: any): void;
declare function resetCharacterSelection(charID: any): void;
declare function toggleCharacterSelectionView(): void;
/**
 *
 * @param {CharacterID} charID
 */
declare function selectCharacter(char: any, confirmed?: boolean): void;
declare function resetAccountData(): void;
declare function rollForRhaelyx(skill: any, offline?: boolean): number | false;
/** Consume Crown Charges if equipped */
declare function removeChargeRhaelyx(isOffline?: boolean): void;
declare function initTooltips(): void;
declare function finaliseLoad(): void;
declare function setupNewCharacter(): void;
declare function checkMediaQuery(mediaQuery: any): boolean;
declare function viewGameGuide(): void;
declare function generateGaussianNumber($mean: any, $stdDev: any): any;
declare function getMean(numActions: any, probability: any): number;
/**
 *
 * @param {string} mediaQuery
 */
declare function getStdDev(numActions: any, probability: any): number;
declare function getMasteryTokenChance(skill: any): number;
declare function dropRingHalfAChance(levelReq: any): number;
declare function getRhaelyxChance(): number;
declare function getRhaelyxStoneChance(): 0 | 0.0001;
declare function getRhaelyxPiece(skill: any): number;
declare function getPetChance(petID: any, timePerAction: any, forceSkill?: boolean): number;
declare function openNextModal(): void;
declare function addModalToQueue(modal: any): void;
declare function getItemMedia(itemID: any): string;
declare function getMiscMedia(localURL: any): string;
declare function setDiscordRPCDetails(): void;
declare function initSteam(): void;
/**
 *
 * @param {ItemID} itemID
 */
declare function checkForSteamAchievements(): void;
declare function unlockSteamAchievement(achievementName: any, i: any): void;
declare function resetSteamAchievements(): void;
declare function cleanSaveFile(): void;
/**
 * Steam specific function
 */
declare function agreeToNotice(noticeID: any): void;
declare function checkForItemsToAddToBank(): void;
declare function animateProgress(div: any, interval: any, stayFull?: boolean): void;
declare function resetProgressAnimation(div: any): void;
declare function rollPercentage(chance: any): boolean;
/**
 * Mobile/steam specific function
 * @param {0} noticeID
 */
declare function applyModifier(baseStat: any, modifier: any, type?: number): any;
declare function binomial_distribution(n: any, p: any, epsilon?: number): number[];
/**
 *
 * @param {string} div
 */
declare function sample_from_binomial(numberTrials: any, chance: any): number;
declare function calculateSkillInterval(
    skillID: any,
    baseInterval: any,
    action?: number,
    useCharge?: boolean
): number;
declare function calculateSkillPreservationChance(
    skillID: any,
    action?: number,
    itemID?: number,
    useCharges?: boolean,
    interval?: number
): number;
declare function calculateChanceToDouble(
    skill: any,
    isCombat?: boolean,
    baseChance?: number,
    action?: number,
    itemID?: number,
    useCharge?: boolean,
    interval?: number
): number;
declare function getNumberMultiplierValue(value: any): number;
declare function createGamemodeSelectionElements(): string;
declare function getGamemodeSelectionRules(gamemode: any): string;
declare function getGamemodeSafety(
    gamemode: any
):
    | '<h5 class="font-w700 font-size-sm mb-1 text-success">This is a safe Gamemode.</h5>'
    | '<h5 class="font-w700 font-size-sm mb-1 text-danger text-uppercase">This mode is permadeath.</h5>';
declare function getGamemodeDescription(gamemode: any): string;
declare function getGamemodeEventStatus(gamemode: any): string;
declare function getGamemodeName(gamemode: any): string;
declare function deleteKeysFromObject(object: any): void;
declare function getAverage(elements?: any[]): number;
declare function buildDataFromItemsArray(): void;
declare function createItemRecipeElement(itemID: any, qty?: number, elementID?: string, qtyStyle?: string): string;
declare function createSummonSynergySearchElement(
    itemID: any,
    qtyStyle?: string,
    itemMedia?: number,
    showQty?: boolean,
    showSkill?: boolean
): string;
declare function getItemRecipeBorder(qtyStyle: any): '' | 'border-item-invalid';
declare function updateItemRecipeBorder(elementID: any, isInvalid?: boolean): void;
declare function getCloudSaveHeartbeatInterval(): number;
declare function startProgressBarTimer(interval: any): void;
/** Updates that happen every 10 seconds */
declare function updateEvery10Seconds(): void;
declare function viewMonsterStats(mID: any): void;
declare function showEnableOfflineCombatModal(): void;
declare function dismissOfflineCombatAlert(): void;
declare function toggleOfflineCombatCheckbox(id: any): void;
declare function updateEnableOfflineCombatBtn(): void;
declare function enableOfflineCombat(): void;
declare const useCDN: true;
declare const CDNVersion: 'v018';
declare const CDNEndpoint: 'https://cdn.melvor.net/core';
declare const DEBUGENABLED: false;
declare var DEBUG_REPORTER: any[];
declare var CDNDIR: string;
/** @type {NumberDictionary<SkillData>} */
declare const SKILLS: NumberDictionary<SkillData>;
declare const pages: string[];
declare const specialEvents: {
    active: boolean;
}[];
declare const gameTitle: 'Melvor Idle :: Alpha v0.21';
/**
 * Save game variable
 * @type {string[]} */
declare var titleNewsID: string[];
/** @type {string[]} */
declare var currentTitleNewsID: string[];
/** @type {{eventName: string;args: PlayFabEventBody}[]} */
declare var playFabEventQueue: {
    eventName: string;
    args: PlayFabEventBody;
}[];
/** Save game variable */
declare var gp: number;
declare var currentPage: number;
declare const us: 'w';
declare const p: 848;
/** Save game variable */
declare var saveTimestamp: number;
/**
 * @deprecated Old save game variable
 * @type {boolean[]} */
declare var itemLog: boolean[];
/**
 * Save game variable
 * @type {ItemStat[]} */
declare var itemStats: ItemStat[];
/** @type {MonsterStat[]} */
declare var monsterStats: MonsterStat[];
declare var isLoaded: boolean;
declare let confirmedLoaded: boolean;
declare var currentlyCatchingUp: boolean;
/** @type {number[]} */
declare var killCount: number[];
declare const tutorialT: 168;
declare const ar: number[];
/**
 * Save game variable
 * @type {Offline} */
declare var offline: Offline;
declare const IItemID: number;
declare var skillsMenu: boolean;
declare var combatMenu: boolean;
declare var easterLoaded: boolean;
declare var offlinePause: boolean;
/**
 * Save game variable
 * @type {GameMode} */
declare var currentGamemode: GameMode;
/** @type {number[]} */
declare var steamAchievements: number[];
declare var connectedToSteam: boolean;
/** @type {TimeoutID|null} */
declare var offlineProgressTimer: TimeoutID | null;
/** @type {TimeoutID|null} */
declare var updateTooltipsTimer: TimeoutID | null;
/** @type {ItemQuantity2[]} */
declare var itemNotifyToProcess: ItemQuantity2[];
/** @type {TimeoutID|null} */
declare var itemNotifyTimer: TimeoutID | null;
declare var offlineActionIsPaused: boolean;
declare var tutorialTipData: {
    id: number;
    title: string;
    titleImg: string;
    description: string;
}[];
/** Save game variable */
declare var tutorialTips: {
    activated: boolean;
}[];
/** @type {TooltipInstances} */
declare var tooltipInstances: TooltipInstances;
/** @type {SumFunction} */
declare const arrSum: SumFunction;
declare var eightSeconds: boolean;
declare var currentView: number;
declare var lolYouDiedGetRekt: boolean;
declare let characterLoading: boolean;
/** Save game variable */
declare var firstTimeLoad: boolean;
declare var skillName: string[];
/** Savegame variable. Stores skill experience. */
declare var skillXP: number[];
/** Savegame variable. Stores skill level. */
declare var skillLevel: number[];
/** Savegame variable */
declare var nextLevelProgress: number[];
declare var skillsUnlocked: any[];
declare var priceToUnlockSkill: number[];
declare var currentSkillLevel: number;
declare var numberMultiplier: number;
declare var inCharacterSelection: boolean;
declare var returnToGameAfterSubmission: boolean;
declare var modalQueue: any[];
declare var cloudSaveHeartbeatLevel: number;
declare var loadingOfflineProgress: boolean;
declare var modalIsOpen: boolean;
declare var offlineProgressCache: any;
declare var marksFoundOffline: number;
declare class Exp {
    table: number[];
    xpSum: number;
    equate(level: any): number;
    level_to_xp(level: any): number;
    xp_to_level(xp: any, level?: number): number;
}
declare const exp: Exp;
declare function selectFromLootTable(skill: any, id: any): number;
declare let cookingID: number;
declare let cookInterval: number;
declare let cooks: number;
declare const cookingBankID: number;
declare let smithItem: any;
declare let smithingInterval: number;
declare let gloveChargesUsed: number;
declare let smithCheck: boolean;
declare let noGloveActions: any;
declare const baseActionXP: number;
declare let npcID: number | number[] | [number, number] | [number, [number, number, number]];
declare let thievingInterval: number;
declare let success: number;
declare let successRate: number;
declare let timeSpent: number;
declare let fletchItem: number;
declare let fletchCount: number;
declare let fletchCheck: boolean;
declare let craftItem: any;
declare let craftCount: number;
declare let craftCheck: boolean;
declare let runecraftItem: any;
declare let runecraftCount: number;
declare let xpMultiplier: number;
declare let runecraftCheck: boolean;
declare let herbloreItem: any;
declare let herbloreCount: number;
declare let herbloreCheck: boolean;
declare const altSpell: Altmagic;
declare let levelReq: number;
declare let altSmithCheck: boolean;
declare let summoningItem: any;
declare let summoningCount: number;
declare let summoningCheck: boolean;
declare let timeGone: number;
declare let goneFor: string;
declare let summoningMarksFound: string;
declare let strengthXPGained: string;
declare let FMXPGained: string;
declare let summoningXPGained: string;
declare let summoningXPAfter: number;
declare const remainingSummonCharges: number[];
declare let youWereGone: string;
declare let creatingAccount: boolean;
declare var progressBarTimer: any;
declare const tenSeconds: 10000;
declare let tenSecondUpdateTimeout: any;
declare var gameVersionChecker: number;
declare var offlineCombatChecks: boolean[];
declare function MasteryExp(): void;
declare class MasteryExp {
    /**
     *
     * @param {number} xp
     */
    equate: (xp: number) => number;
    /**
     *
     * @param {number} level
     */
    level_to_xp: (level: number) => number;
    /**
     *
     * @param {number} xp
     * @param {number} level
     */
    xp_to_level: (xp: number, level?: number) => number;
}
declare function loadMasteryTab(): void;
/**
 *
 * @param {SkillID} skill
 */
declare function updateTotalMastery(skill: SkillID): void;
/**
 * onclick callback function (used in href)
 * handle the display of mastery progress
 * @param {SkillID} skill
 */
declare function showMasteryUnlocks(skill: SkillID): void;
/** @type {number[]} */
declare var currentMastery: number[];
/** @type {number[]} */
declare var totalMastery: number[];
declare const na: 't';
declare var masteryExp: MasteryExp;
/** @type {NumberDictionary<MasteryUnlock[]>} */
declare var masteryUnlocks: NumberDictionary<MasteryUnlock[]>;
declare function populateMasteryObject(): void;
declare function createMasteryPoolElements(): void;
/**
 *
 * @param {SkillID} skill
 * @param {boolean} [mainButtons=true]
 */
declare function getMasteryPoolElement(skill: SkillID, mainButtons?: boolean): string;
/**
 *
 * @param {SkillID} skill
 * @param {number} id
 */
declare function createVisualMasteryElement(skill: SkillID, id: number): string;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function getMasteryLevel(skill: SkillID, masteryID: number): number;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function getMasteryProgressXP(skill: SkillID, masteryID: number): any;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function updateMasteryLevel(skill: SkillID, masteryID: number): void;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function updateMasteryProgress(skill: SkillID, masteryID: number): void;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function processUpdateMasteryProgress(skill: SkillID, masteryID: number): void;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function getMasteryProgress(skill: SkillID, masteryID: number): number;
/**
 *
 * @param {SkillID} skill
 */
declare function getMasteryPoolProgress(skill: SkillID): number;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 * @param {number} timePerAction
 */
declare function getMasteryXpToAdd(skill: SkillID, masteryID: number, timePerAction: number): number;
/**
 *
 * @param {SkillID} skill
 */
declare function getTotalUnlockedItems(skill: SkillID): number;
/**
 *
 * @param {SkillID} skill
 */
declare function getCurrentTotalMasteryLevelForSkill(skill: SkillID): number;
/**
 *
 * @param {SkillID} skill
 */
declare function getTotalMasteryLevelForSkill(skill: SkillID): number;
/**
 *
 * @param {SkillID} skill
 */
declare function getTotalItemsInSkill(skill: SkillID): number;
/**
 *
 * @param {SkillID} skill
 * @param {number} defaultTime
 * @param {number} [masteryID=0]
 */
declare function getTimePerActionModifierMastery(skill: SkillID, defaultTime: number, masteryID?: number): number;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 * @param {number} timePerAction
 * @param {boolean} [spendingXP=false]
 * @param {number} xp
 * @param {boolean} [addToPool=true]
 * @param {boolean} [offline=false]
 */
declare function addMasteryXP(
    skill: SkillID,
    masteryID: number,
    timePerAction: number,
    spendingXP?: boolean,
    xp?: number,
    addToPool?: boolean,
    offline?: boolean
): void;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 * @param {boolean} [offline=false]
 */
declare function notifyMasteryLevelUp(skill: SkillID, masteryID: number, offline?: boolean): void;
/**
 *
 * @param {SkillID} skill
 */
declare function getMasteryPoolTotalXP(skill: SkillID): number;
/**
 *
 * @param {SkillID} skill
 * @param {number} xp
 * @param {boolean} token
 * @returns {number}
 */
declare function getMasteryXpToAddToPool(skill: SkillID, xp: number, token: boolean): number;
/**
 *
 * @param {SkillID} skill
 * @param {number} xp
 * @param {boolean} [offline=false]
 * @param {boolean} [token=false]
 */
declare function addMasteryXPToPool(skill: SkillID, xp: number, offline?: boolean, token?: boolean): void;
/**
 *
 * @param {SkillID} skill
 */
declare function updateMasteryPoolProgress(skill: SkillID): void;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function getMasteryImage(skill: SkillID, masteryID: number): string;
/**
 *
 * @param {SkillID} skill
 * @param {number} masteryID
 */
declare function getMasteryName(skill: SkillID, masteryID: number): string;
/**
 *
 * @param {SkillID} skill
 */
declare function showSpendMasteryXP(skill: SkillID): void;
declare function setMasteryPoolLevelUp(skill: any, level: any): void;
declare function updateSpendMasteryScreen(skill: any, masteryID: any): void;
declare function getMasteryXpForNextLevel(skill: any, masteryID: any): number;
declare function levelUpMasteryWithPool(skill: any, masteryID: any, confirmation?: boolean): void;
declare function viewMasteryCheckpoints(skill: any): void;
declare function getMasteryCheckpointBonusStatus(skill: any, checkpoint: any): string;
declare function getCheckpointBonusClass(skill: any, checkpoint: any): string;
declare function toggleHideMaxLevel(skill: any): void;
declare function sortMasteryView(skill: any): void;
declare function updateMasteryLevelCache(skill: any, masteryID: any): void;
declare function populateMasteryLevelCache(): void;
/**
 * Save game variable
 * @type {Mastery} */
declare var MASTERY: Mastery;
/** @type {MasteryLevelCache} */
declare var masteryLevelCache: MasteryLevelCache;
/** @type {TimeoutID} */
declare let masteryUpdateTimer: TimeoutID;
/** @type {NumberDictionary<MasteryPoolBonus>} */
declare const masteryCheckpointBonuses: NumberDictionary<MasteryPoolBonus>;
declare let masteryPoolLevelUp: number;
declare const masteryCheckpoints: number[];
/** @type {NumberDictionary<number[]>} */
declare var masterySorted: NumberDictionary<number[]>;
/**
 * @type {MasteryCache}
 */
declare var masteryCache: MasteryCache;
/**
 * @type {{skill: SkillID, masteryID: number}[]}
 */
declare var masteryUpdatesToProcess: {
    skill: SkillID;
    masteryID: number;
}[];
/** @type {SkillObject<Milestone[]>} */
declare const MILESTONES: SkillObject<Milestone[]>;
declare function loadPets(): void;
/**
 *
 * @param {PetID} petID
 * @param {boolean} [gameLoading=false]
 */
declare function updatePet(petID: PetID, gameLoading?: boolean): void;
/**
 *
 * @param {PetID} petID
 * @param {number} timePerAction
 * @param {boolean} [offline=false]
 * @param {false|SkillID} [forceSkill=false]
 */
declare function rollForPet(
    petID: PetID,
    timePerAction?: number,
    offline?: boolean,
    forceSkill?: false | SkillID
): boolean;
/**
 *
 * @param {PetID} petID
 * @param {boolean} [offline=false]
 */
declare function unlockPet(petID: PetID, offline?: boolean): void;
/**
 *
 * @param {PetID} petID
 */
declare function getPetUnlockModal(petID: PetID): {
    title: string;
    html: string;
    imageUrl: string;
    imageWidth: number;
    imageHeight: number;
    imageAlt: string;
};
/**
 * onClick callback function
 * @param {PetID} petID
 */
declare function petPet(petID: PetID): void;
/**
 *
 * @param {PetID} petID
 * @param {DungeonID} dungeonID
 */
declare function rollForPetDungeonPet(petID: PetID, dungeonID: DungeonID): void;
/** @type {Pet[]} */
declare const PETS: Pet[];
/**
 * Save game variable
 * @type {boolean[]} */
declare var petUnlocked: boolean[];
declare var petXPToLevel: number;
/**
 *
 * @param {MiningID} ore
 * @param {boolean} [clicked=false]
 * @param {boolean} [ignoreDepletion=false]
 */
declare function mineRock(ore: MiningID, clicked?: boolean, ignoreDepletion?: boolean): void;
/**
 *
 * @param {number} ore
 * @returns {number}
 */
declare function getMiningGemChance(ore: number): number;
declare function loadMiningOres(): void;
declare function updateMiningOres(): void;
declare function updateOreLevelRequirements(): void;
/**
 *
 * @param {number} ore
 * @returns {number}
 */
declare function calculateIncreasedMiningMaxHP(ore: number): number;
/**
 *
 * @param {MiningID} ore
 * @param {boolean} [initialise=false]
 */
declare function updateRockHP(ore: MiningID, initialise?: boolean): void;
/**
 *
 * @param {MiningID} ore
 */
declare function rockReset(ore: MiningID): void;
declare function canMineDragonite(): boolean;
declare function collectGem(offline?: boolean, rollDouble?: boolean): number;
declare function updateMiningRates(): void;
declare const miningData: {
    level: number;
    respawnInterval: number;
    ore: number;
    masteryID: number;
}[];
/**
 * Save game variable
 * @type {RockData[]} */
declare var rockData: RockData[];
declare const oreTypes: string[];
declare var isMining: boolean;
/** @type {MiningID|null} */
declare var currentRock: MiningID | null;
/** @type {TimeoutID|null} */
declare var miningTimeout: TimeoutID | null;
declare const baseMiningInterval: 3000;
declare const baseRockHP: 5;
declare function startRunecrafting(clicked?: boolean): void;
/**
 *
 * @param {RunecraftingID} runecraftingID
 * @param {boolean} [update=false]
 */
declare function selectRunecraft(runecraftingID: RunecraftingID, update?: boolean): void;
declare function loadRunecrafting(): void;
declare function updateRunecrafting(): void;
declare function getRunecraftingRecipeQty(itemID: any, recipeIndex: any): number;
declare function updateRunecraftQty(itemID: any): void;
declare function checkRunecraftingReq(itemID: any): boolean;
declare function runecraftingCategory(cat: any): void;
/** @type {RunecraftingItem[]} */
declare var runecraftingItems: RunecraftingItem[];
/** @type {RunecraftingID|null} */
declare var currentRunecraft: RunecraftingID | null;
declare var isRunecrafting: boolean;
/** @type {TimeoutID|null} */
declare var runecraftingTimeout: TimeoutID | null;
/** @type {RunecraftingID|null} */
declare var selectedRunecraft: RunecraftingID | null;
/** @type {QtyReqCheck[]} */
declare var runecraftReqCheck: QtyReqCheck[];
declare var runecraftInterval: number;
/** @type {RunecraftingItem[]} */
declare var runecraftingSorted: RunecraftingItem[];
declare const elementals: number[];
declare const combinations: number[];
declare function disableSidebarSwipeTimer(): void;
declare function updateKeys(): void;
/**
 *
 * @param {number} charID
 * @returns {string}
 */
declare function getKeysForCharacter(charID: number): string;
/**
 *
 * @param {string} key
 * @param {*} value
 */
declare function setItem(key: string, value: any): void;
/**
 *
 * @param {string} key
 */
declare function getItem(key: string): any;
/**
 *
 * @param {string} key
 */
declare function removeItem(key: string): void;
/**
 *
 * @param {'all'|'offline'|true} vars
 */
declare function saveData(vars?: 'all' | 'offline' | true): void;
/**
 *
 * @param {string} keyPrefix
 * @returns {NewSaveGame}
 */
declare function getSaveGameOld(keyPrefix: string): NewSaveGame;
/**
 *
 * @param {string} keyPrefix
 */
declare function removeSaveOld(keyPrefix: string): void;
/**
 * Gets a saveGame object of the local save
 * @param {string} keyPrefix
 * @returns {{saveGame: NewSaveGame; oldFormat: boolean}}
 */
declare function getLocalSave(keyPrefix: string): {
    saveGame: NewSaveGame;
    oldFormat: boolean;
};
/**
 * Checks if the local save with the key exists
 * @param {string} keyPrefix
 * @returns {0|1|2} 0: No save, 1: Old format save, 2: new format save
 */
declare function doesLocalSaveExist(keyPrefix: string): 0 | 1 | 2;
declare function loadData(): boolean;
declare function deleteData(characterID?: number): void;
/**
 * onClick callback function
 * @param {boolean} [update=false]
 */
declare function exportSave(update?: boolean): void;
/**
 *
 * @param {SaveString|false} [forceSaveToImport=false]
 * @param {CharacterID} characterID
 */
declare function importSave(forceSaveToImport?: SaveString | false, characterID?: CharacterID): boolean;
/**
 * onClick callback function
 * @param {CharacterID} characterID
 */
declare function openImportSave(characterID?: CharacterID): void;
declare function openExportSave(characterID: any): void;
declare function openDownloadSave(characterID: any): void;
declare function openCharacterSelectionSettings(characterID?: number, characterName?: string): void;
declare function loadGame(update?: boolean): void;
/**
 *
 * @param {boolean} [customKey=false]
 * @param {number} charID
 * @returns {string}
 */
declare function getSave(customKey?: boolean, charID?: number): string;
/**
 *
 * @param {string} keyPrefix
 * @returns {string}
 */
declare function getSaveStringOld(keyPrefix: string): string;
declare function downloadSave(backup?: boolean, save?: number): boolean;
declare function loadGameRaw(saveString: any): void;
/**
 *
 * @param {NewSaveGame} savegame
 */
declare function setGlobalsFromSaveGame(savegame: NewSaveGame): void;
declare function getSaveJSON(save: any): any;
declare function confirmLoadLocalSave(): void;
declare function fixMySave(confirmed?: boolean): void;
declare function setBackupSaveDetails(save: any): void;
declare function getItemFromSave(saveString: any, varName: any): any;
declare function setForceReload(character: any, reloadNow?: boolean): void;
declare function removeForceReload(): void;
declare function onloadEvent(accessCheck?: boolean, resetPage?: boolean): void;
declare function resetEntirePage(accessCheck?: boolean, startCombatLoad?: boolean): void;
declare function handleBankSidebarScroll(): void;
declare function changePageCharacterSelection(page: any): void;
declare function setStartingGamemode(gamemode: any): void;
declare function resetVariablesToDefault(): void;
declare var isTest: boolean;
/** @type {CharacterID} */
declare var currentCharacter: CharacterID;
declare var characterSelected: boolean;
/** @type {SaveString|null} */
declare var backupSave: SaveString | null;
declare var dataDeleted: boolean;
declare var keyVersion: string;
declare var keyTest: string;
declare var key: string;
declare var disableSidebarSwipe: boolean;
declare var currentStartPage: number;
/** @type {TimeoutID|null} */
declare var sidebarSwipeTimer: TimeoutID | null;
/** @type {GameMode} */
declare var startingGamemode: GameMode;
declare var panVal: number;
declare let firstSkillAction: boolean;
/**
 * Mobile/Steam Specific Function
 * @param {number} zoomLevel
 */
declare function adjustZoom(zoomLevel: number): void;
/**
 * onClick callback function
 * @param {27|32} setting
 * @param {boolean} [ignore=false]
 */
declare function toggleSetting(setting: 27 | 32, ignore?: boolean): void;
/**
 *
 * @param {1|2|3|4|5|6|7|8|9|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|28|29|30|31|33|34|35} setting
 * @param {boolean|number} value
 * @param {boolean} [ignore=false]
 */
declare function changeSetting(
    setting:
        | 1
        | 2
        | 3
        | 4
        | 5
        | 6
        | 7
        | 8
        | 9
        | 12
        | 13
        | 14
        | 15
        | 16
        | 17
        | 18
        | 19
        | 20
        | 21
        | 22
        | 23
        | 24
        | 25
        | 26
        | 28
        | 29
        | 30
        | 31
        | 33
        | 34
        | 35,
    value: boolean | number,
    ignore?: boolean
): void;
declare function loadCurrentSettings(): void;
declare function toggleAutoPotion(update?: boolean): void;
declare function setDefaultSettings(): void;
declare namespace SETTINGS {
    namespace bank {
        const bankBorder: number;
        const currentEquipDefault: boolean;
        const defaultBankSort: number;
        const defaultItemTab: BankDefaultItem[];
    }
    namespace mastery {
        const hideMaxLevel: boolean;
        const confirmationCheckpoint: boolean;
    }
    namespace general {
        const pushNotificationOffline: boolean;
        const pushNotificationFarming: boolean;
        const enabledOfflineCombat: boolean;
        const enableNeutralSpecModifiers: boolean;
    }
    namespace notifications {
        const combatStunned: boolean;
        const combatSleep: boolean;
        const summoningMark: boolean;
    }
    namespace performance {
        const disableDamageSplashes: boolean;
        const disableProgressBars: boolean;
    }
}
/** Save game variable */
declare var ignoreBankFull: boolean;
/** Save game variable */
declare var defaultPageOnLoad: number;
/** Save game variable */
declare var levelUpScreen: number;
/** Save game variable */
declare var continueThievingOnStun: boolean;
/** Save game variable */
declare var showItemNotify: number;
/** Save game variable */
declare var showGPNotify: boolean;
/** Save game variable */
declare var autoRestartDungeon: boolean;
/** Save game variable */
declare var autoSaveCloud: boolean;
/** Save game variable */
declare var darkMode: boolean;
/** Save game variable */
declare var showEnemySkillLevels: boolean;
/** Save game variable */
declare var confirmationOnClose: boolean;
/** Save game variable */
declare var enableAccessibility: boolean;
/** Save game variable */
declare var autoPotion: boolean;
/** @deprecated Old save variable */
declare var autoUseSpecialAttack: boolean;
/** @deprecated Old save variable */
declare var showHPNotify: boolean;
declare const autoSlayerTask: true;
/** Save game variable */
declare var showCommas: boolean;
/** Save game variable */
declare var showVirtualLevels: boolean;
/** Save game variable */
declare var formatNumberSetting: number;
/** Save game variable */
declare var pauseOfflineActions: boolean;
/** Save game variable */
declare var showSaleNotifications: boolean;
/** Save game variable */
declare var showShopNotifications: boolean;
/** Save game variable */
declare var showCombatMinibar: boolean;
/** Save game variable */
declare var showCombatMinibarCombat: boolean;
/** Save game variable */
declare var useCombinationRunes: boolean;
/** Save game variable */
declare var showSkillMinibar: boolean;
/** Save game variable */
declare var disableAds: boolean;
/**
 *
 * @param {GloveID} gloves
 * @param {boolean} [offline=false]
 * @param {number} [qty=1]
 */
declare function removeGloveCharge(gloves: GloveID, offline?: boolean, qty?: number): void;
/**
 *
 * @param {GloveID} gloves
 * @returns {boolean}
 */
declare function isUsingGloves(gloves: GloveID): boolean;
declare function BankUpgradeCost(): void;
declare class BankUpgradeCost {
    /**
     *
     * @param {number} gp
     */
    equate: (gp: number) => number;
    /**
     *
     * @param {number} level
     */
    level_to_gp: (level: number) => number;
    /**
     *
     * @param {number} gp
     */
    gp_to_level: (gp: number) => number;
}
declare function NewNewBankUpgradeCost(): void;
declare class NewNewBankUpgradeCost {
    /**
     *
     * @param {number} level
     */
    equate: (level: number) => number;
    /**
     *
     * @param {number} level
     */
    level_to_gp: (level: number) => number;
}
declare function NewBankUpgradeCost(): void;
declare class NewBankUpgradeCost {
    /**
     *
     * @param {number} gp
     */
    equate: (gp: number) => number;
    /**
     *
     * @param {number} level
     */
    level_to_gp: (level: number) => number;
    /**
     *
     * @param {number} gp
     */
    gp_to_level: (gp: number) => number;
}
declare function getBankUpgradeCost(): number;
/**
 *
 * @param {'axe'|'all'|'gloves'|'godUpgrades'|'autoeat'|'skillcapes'|'autoSlayer'|'pickaxe'|'multitree'|'cookingFire'|'equipmentSet'|'rod'|'bank'|'cookingRange'|'compost'|'feathers'|'fire'} identifier
 * @param {boolean} [updateCosts=true]
 */
declare function updateShop(
    identifier:
        | 'axe'
        | 'all'
        | 'gloves'
        | 'godUpgrades'
        | 'autoeat'
        | 'skillcapes'
        | 'autoSlayer'
        | 'pickaxe'
        | 'multitree'
        | 'cookingFire'
        | 'equipmentSet'
        | 'rod'
        | 'bank'
        | 'cookingRange'
        | 'compost'
        | 'feathers'
        | 'fire',
    updateCosts?: boolean
): void;
declare function getCurrentPickaxe(): string;
declare function getCurrentFishingRod(): string;
declare function getCurrentPickaxeDescription(): string;
declare function getCurrentFishingRodDescription(): string;
declare function getCurrentAxe(): string;
declare function getCurrentAxeDescription(): string;
/**
 *
 * @param {string} qty
 */
declare function updateBuyQty(qty: string): void;
/**
 * Maybe not deprecated? used in buyShopItem
 * @param {GloveID} gloves
 */
declare function buyGloves(gloves: GloveID): void;
/**
 *
 * @param {ShopCategory} cat
 * @param {number} id
 * @param {number} [qty=1]
 */
declare function addShopPurchase(cat: ShopCategory, id: number, qty?: number): void;
/**
 *
 * @param {keyof Shop} category
 * @param {number} id
 * @param {boolean} [confirmed=false]
 */
declare function buyShopItem(category: keyof Shop, id: number, confirmed?: boolean): void;
declare function updateShopCosts(): void;
declare function updateSlayerTaskShopCosts(): void;
/**
 *
 * @param {ShopCategoryData} shopItem
 * @returns {boolean}
 */
declare function checkShopUnlockRequirements(shopItem: ShopCategoryData): boolean;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 * @param {string} [textClass='text-danger']
 * @param {boolean} [quickBuy=false]
 * @returns {string}
 */
declare function getShopUnlockHTML(
    category: ShopCategory,
    id: number,
    textClass?: string,
    quickBuy?: boolean
): string;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 * @param {string} textClass
 * @param {boolean} [quickBuy=false]
 */
declare function updateShopItemUnlock(
    category: ShopCategory,
    id: number,
    textClass: string,
    quickBuy?: boolean
): void;
/**
 *
 * @param {string} type
 * @param {ShopCategory} category
 * @param {number} id
 * @param {string} textClass
 * @param {boolean} [quickBuy=false]
 */
declare function updateShopItemUnlockElement(
    type: string,
    category: ShopCategory,
    id: number,
    textClass: string,
    quickBuy?: boolean
): void;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 * @returns {boolean}
 */
declare function checkShopItemPurchased(category: ShopCategory, id: number): boolean;
/**
 *
 * @param {ShopCostTypes} type
 * @param {number} amount
 * @param {ItemID} [itemID=0]
 * @param {boolean} [hasQty=false]
 */
declare function getShopCostClass(type: ShopCostTypes, amount: number, itemID?: ItemID, hasQty?: boolean): string;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 */
declare function checkShopBuyLimit(category: ShopCategory, id: number): boolean;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 * @param {boolean} [quickBuy=false]
 * @returns {string}
 */
declare function getShopCostHTML(category: ShopCategory, id: number, quickBuy?: boolean): string;
/**
 *
 * @param {string} type
 * @param {ShopCategory} category
 * @param {number} id
 * @param {boolean} [quickBuy=false]
 * @returns {string}
 */
declare function shopCostID(type: string, category: ShopCategory, id: number, quickBuy?: boolean): string;
/**
 *
 * @param {string} type
 * @param {ShopCategory} category
 * @param {number} id
 * @param {boolean} [quickBuy=false]
 * @returns {string}
 */
declare function shopUnlockID(type: string, category: ShopCategory, id: number, quickBuy?: boolean): string;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 * @param {string} media
 * @param {string} type
 * @param {number} cost
 * @param {ItemID} itemID
 * @param {boolean} hasQty
 * @param {boolean} [quickBuy=false]
 * @returns {string}
 */
declare function getShopCostElementHTML(
    category: ShopCategory,
    id: number,
    media: string,
    type: string,
    cost: number,
    itemID: ItemID,
    hasQty: boolean,
    quickBuy?: boolean
): string;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 * @param {string} type
 * @param {number} cost
 * @param {number} qty
 * @param {boolean} hasQty
 * @param {ItemID} itemID
 * @param {boolean} [quickBuy=false]
 */
declare function updateShopCostElement(
    category: ShopCategory,
    id: number,
    type: string,
    cost: number,
    qty: number,
    hasQty: boolean,
    itemID?: ItemID,
    quickBuy?: boolean
): void;
/**
 *
 * @param {ShopCategory} category
 * @param {number} id
 * @param {boolean} [quickBuy=false]
 * @returns {string}
 */
declare function getShopUnlockAndCostHTML(category: ShopCategory, id: number, quickBuy?: boolean): string;
/**
 *
 * @param {ShopCategoryData} shopItem
 * @returns {string}
 */
declare function getShopUnlockClass(shopItem: ShopCategoryData): string;
declare function createShop(): void;
/**
 *
 * @param {ShopCategory} category
 * @param {number} i
 */
declare function createShopCategory(category: ShopCategory, i: number): string;
/**
 *
 * @param {number} catID
 * @param {number} id
 * @param {boolean} [quickBuy=false]
 * @returns {string}
 */
declare function createShopItem(catID: number, id: number, quickBuy?: boolean): string;
/**
 * onclick callback
 * @param {0|1|2|4} menu
 */
declare function toggleShopMenu(menu: 0 | 1 | 2 | 4): void;
/**
 *
 * @param {0|1|2|3|4|5|6} cat
 */
declare function shopCategory(cat?: 0 | 1 | 2 | 3 | 4 | 5 | 6): void;
/**
 *
 * @param {ItemID} itemID
 */
declare function quickBuyItem(itemID: ItemID): void;
declare function processQuickBuyItem(): void;
declare function checkForDuplicateShopPurchases(): void;
/**
 *
 * @param {ItemID} itemID
 * @returns {[number,number]}
 */
declare function isItemInShop(itemID: ItemID): [number, number];
/** @type {Record<ShopCategory,ShopCategoryData[]>} */
declare const SHOP: Record<ShopCategory, ShopCategoryData[]>;
/** @type {Map<string, ShopPurchase>} */
declare var shopItemsPurchased: Map<string, ShopPurchase>;
/** Save game variable */
declare var buyQty: number;
/** Save game variable */
declare var autoSlayer: boolean;
declare var bankUpgradeCost: BankUpgradeCost;
declare var newBankUpgradeCost: NewBankUpgradeCost;
declare var newNewBankUpgradeCost: NewNewBankUpgradeCost;
/** @type {NumberDictionary<[number,number]>} */
declare var isInShopCache: NumberDictionary<[number, number]>;
declare var quickBuyItemID: number;
declare const skillcapeItems: number[];
declare const glovesCost: number[];
declare const glovesActions: number[];
declare const gloveID: number[];
declare const gloveSkills: number[];
/** Save game variable */
declare var glovesTracker: {
    name: string;
    isActive: boolean;
    remainingActions: number;
}[];
declare const godUpgradeData: {
    name: string;
    description: string;
    cost: number;
    dungeonID: number;
    media: string;
}[];
declare function startSmithing(clicked?: boolean): void;
/**
 *
 * @param {number} smithingID
 * @param {boolean} [update=false]
 */
declare function selectSmith(smithingID: number, update?: boolean): void;
declare function loadSmithing(): void;
declare function updateSmithing(): void;
declare function updateSmithQty(itemID: any): void;
declare function smithCategory(cat: any): void;
declare function smithingRecipeContainsItem(smithingItem: any, itemToCheck: any): boolean;
declare function getSmithingRecipeQty(itemID: any, recipeIndex: any, useCharges?: boolean): number;
declare function checkSmithingReq(itemID: any, ignoreCoal?: boolean): boolean;
/** @type {SmithingItem[]} */
declare var smithingItems: SmithingItem[];
/** @type {SmithingItem[]} */
declare var smithingSorted: SmithingItem[];
/** @type {SmithingID|null} */
declare var currentSmith: SmithingID | null;
declare var isSmithing: boolean;
/** @type {TimeoutID|null} */
declare var smithingTimeout: TimeoutID | null;
/**  @type {QtyReqCheck[]} */
declare var smithReqCheck: QtyReqCheck[];
/** @type {SmithingID|null} */
declare var selectedSmith: SmithingID | null;
declare var smithInterval: number;
/**
 *
 * @param {string} key
 * @param {string} [text='']
 * @param {number} [sendAfter=0]
 */
declare function sendPushNotification(key: string, text?: string, sendAfter?: number): void;
/**
 *
 * @param {string} key
 * @param {string} pushID
 */
declare function storeScheduledPushNotification(key: string, pushID: string): void;
/**
 *
 * @param {string} key
 */
declare function deleteScheduledPushNotification(key: string): void;
/**
 *
 * @param {string} key
 * @returns {boolean}
 */
declare function checkForExistingScheduledPushNotification(key: string): boolean;
declare function connectDevicePushNotifications(): void;
declare function disconnectDevicePushNotifications(): void;
declare function getConnectedPushNotificationDevice(): void;
declare function getPushNotificationPlatform(): string;
/**
 *
 * @returns {Promise<string>}
 */
declare function getPushNotificationToken(): Promise<string>;
/**
 *
 * @param {string} token
 */
declare function storePushNotificationToken(token: string): void;
/**
 *
 * @param {string} platform
 */
declare function storePushNotificationPlatform(platform: string): void;
/**
 * Save game variable
 * @type {StringDictionary<string>} */
declare var scheduledPushNotifications: StringDictionary<string>;
/** @type {StringDictionary<string>} */
declare var storedPushNotificationData: StringDictionary<string>;
/**
 *
 * @param {null|'general'|'woodcutting'|'fishing'|'firemaking'|'cooking'|'mining'|'smithing'|'combat'|'thieving'|'farming'|'fletching'|'crafting'|'runecrafting'|'herblore'|'summoning'} skill
 */
declare function updateStats(
    skill?:
        | null
        | 'general'
        | 'woodcutting'
        | 'fishing'
        | 'firemaking'
        | 'cooking'
        | 'mining'
        | 'smithing'
        | 'combat'
        | 'thieving'
        | 'farming'
        | 'fletching'
        | 'crafting'
        | 'runecrafting'
        | 'herblore'
        | 'summoning'
): void;
declare function convertItemLog(): void;
declare function convertMonsterLog(): void;
/**
 *
 * @param {null|ItemID} item
 * @param {number} [quantity=0]
 */
declare function updateItemLog(item?: null | ItemID, quantity?: number): void;
/**
 * onClick callback function
 */
declare function openItemLog(): void;
declare function updateBankItemStats(itemID: any): void;
declare function openMonsterLog(): void;
declare function openPetLog(): void;
declare function updateCompletionLog(): void;
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsGeneral: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsCombat: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsWoodcutting: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsFishing: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsFiremaking: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsCooking: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsMining: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsSmithing: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsThieving: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsFarming: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsFletching: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsCrafting: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsRunecrafting: GameStat[];
/**
 * Save game variable
 * @type {GameStat[]} */
declare var statsHerblore: GameStat[];
declare var completionStats: number;
/**
 *
 * @param {boolean} [clicked=false]
 */
declare function createSummon(clicked?: boolean): void;
declare function loadSummoning(): void;
declare function updateSummoning(): void;
declare function selectSummon(summonID: any, recipe?: number, update?: boolean): void;
declare function getSummoningCreationQty(itemID: any): number;
declare function updateSummonRecipeSelection(itemID: any): boolean;
declare function getSummoningRecipeQty(itemID: any, recipeID: any, recipeItemIndex: any): number;
declare function updateSummoningQty(itemID: any): void;
declare function checkSummoningReq(itemID: any): boolean;
declare function isSynergyUnlocked(summon1: any, summon2: any): boolean;
declare function getSummonSynergyModifiers(
    summon1: any,
    summon2: any
): Partial<ModifierObject<SkillModifierData[], number>>;
declare function getSummonSynergyEnemyModifiers(summon1: any, summon2: any): Partial<CombatModifierObject<number>>;
declare function getChanceForMark(summonID: any, interval: any, skill: any, masteryID: any): number;
declare function rollForSummoningMark(
    skill: any,
    summonID: any,
    interval: any,
    masteryID: any,
    render?: boolean
): boolean;
declare function rollAllPossibleSummoningMarks(
    skill: any,
    interval: any,
    masteryID?: number,
    useCharge?: boolean,
    render?: boolean
): any[];
declare function checkSummoningSynergyActive(
    summon1: any,
    summon2: any,
    useCharge?: boolean,
    interval?: number
): boolean;
declare function useSummonCharge(summonID: any, chargesUsed?: number, interval?: number): void;
declare function getBaseSummoningXP(summonID: any, useCharge?: boolean, interval?: number): number;
declare function discoverMark(summonID: any, render?: boolean): void;
declare function getMarkDiscoveredModal(summonID: any): {
    title: string;
    html: string;
    imageUrl: string;
    imageWidth: number;
    imageHeight: number;
    imageAlt: string;
};
declare function getMarkLevelUpModal(summonID: any): {
    title: string;
    html: string;
    imageUrl: string;
    imageWidth: number;
    imageHeight: number;
    imageAlt: string;
};
declare function createMarkDiscoveryElements(): string;
declare function getMarkDiscoveryElement(summonID: any): string;
declare function updateMarkDiscoveryElement(summonID: any): void;
declare function createMarkDiscoveryProgressElement(summonID: any): string;
declare function quickCreateTablet(summonID: any): void;
declare function getSummoningMarkImg(summonID: any): string;
declare function getSummoningMarkStatus(summonID: any): string;
declare function getSummoningMarkStatusClass(summonID: any): 'text-warning' | 'text-danger' | 'text-success';
declare function getSummoningMarkName(summonID: any): string;
declare function getSummoningFamiliarName(summonID: any): string;
declare function getSummoningMarkProgress(summonID: any): number;
declare function updateSummoningMarkProgress(summonID: any): void;
declare function getSummoningMarkLevel(summonID: any): 1 | 0 | 2 | 3 | 4;
declare function getSummoningMarkSkills(summonID: any): string;
declare function summoningCategory(cat: any): void;
declare function createSynergiesBreakdown(): string;
declare function updateSynergyFamiliarFilter(): void;
declare function filterSynergyFamiliar(summonID: any): void;
declare function quickEquipSynergy(summon1: any, summon2: any): void;
declare function getSynergiesBreakdown(summonID: any, otherSummonID: any): string;
declare function updateSynergiesBreakdown(summonID: any): void;
declare function openSynergiesBreakdown(): void;
declare function displayUnlockedSynergies(): void;
declare function updateSummoningSynergySearch(search: any): void;
declare function updateSummoningSynergySearchArray(): void;
declare function checkForSummoningPet(): void;
/** @type {SummoningData} */
declare const SUMMONING: SummoningData;
/** @type {PlayerSummoningData} */
declare var summoningData: PlayerSummoningData;
/** @type {NumberDictionary<number[]>} */
declare var summoningMarkSkills: NumberDictionary<number[]>;
/** @type {SummoningItem[]} */
declare var summoningItems: SummoningItem[];
/** @type {number|null} */
declare var selectedSummon: number | null;
/** @type {QtyReqCheck[]} */
declare var summoningReqCheck: QtyReqCheck[];
declare var isSummoning: boolean;
/** @type {number|null} */
declare var currentSummon: number | null;
/** @type {TimeoutID|null} */
declare var summoningTimeout: TimeoutID | null;
/** @type {SummoningSearch[]} */
declare var summoningSynergySearch: SummoningSearch[];
/** @type {[number,number]} */
declare var chargesUsedOffline: [number, number];
/** @type {TimeoutID|null} */
declare var updateSummoningTimer: TimeoutID | null;
/**
 *
 * @param {ThievingID} npc
 * @param {boolean} [clicked=true]
 */
declare function pickpocket(npc: ThievingID, clicked?: boolean): void;
/** Returns the % chance to recieve Bobby's pocket */
declare function getBobbysChance(): number;
/** Returns the % chance to recieve Chapeau noir */
declare function getChapsChance(): number;
declare function updateVisualSuccess(): void;
declare function loadThieving(): void;
declare function updateThieving(): void;
/**
 *
 * @param {ThievingID} npcID
 * @param {boolean} [useGloves=true]
 * @param {boolean} [offline=false]
 */
declare function getSuccessRate(npcID: ThievingID, useGloves?: boolean, offline?: boolean): number;
/**
 *
 * @param {number} npcID
 * @param {number} [masteryLevel=-1]
 * @param {number} [interval=1000]
 * @returns {number}
 */
declare function calculateThievingGP(npcID: number, masteryLevel?: number, interval?: number): number;
declare function updateGameTitle(): void;
declare const thievingNPC: {
    name: string;
    level: number;
    xp: number;
    maxHit: number;
    baseSuccess: number;
    maxSuccess: number;
    maxCoins: number;
    lootTable: number[][];
    media: string;
}[];
declare const baseThievingInterval: 3000;
declare const thievingStunInterval: 3000;
declare var isThieving: boolean;
/** @type {ThievingID|null} */
declare var npcID: ThievingID | null;
/** @type {TimeoutID|null} */
declare var thievingTimer: TimeoutID | null;
declare var isStunned: boolean;
declare function getWoodcuttingInterval(): number;
declare function getBaseWoodcuttingInterval(): number;
/**
 *
 * @param {number} tree
 * @returns {number}
 */
declare function getWoodcuttingMultiplier(tree: number): number;
/**
 *
 * @param {number} cutInterval
 */
declare function getWoodcuttingGrants(cutInterval: number): void;
/**
 *
 * @param {WoodcuttingID} treeID
 * @param {number|boolean} [ignore=0]
 */
declare function cutTree(treeID: WoodcuttingID, ignore?: number | boolean): void;
/**
 *
 * @param {number} cutInterval
 * @returns {number}
 */
declare function getBirdNestDropChance(cutInterval: number): number;
/**
 *
 * @param {number} [notify] Unused Parameter
 */
declare function updateWCMilestone(notify?: number): void;
declare function updateWCRates(): void;
declare const trees: {
    type: string;
    level: number;
    interval: number;
    xp: number;
    media: string;
}[];
declare var currentlyCutting: number;
declare const baseCutLimit: 1;
/** @type {WoodcuttingID[]} */
declare var currentTrees: WoodcuttingID[];
/** @type {(TimeoutID|null)[]} */
declare var treeCuttingHandler: (TimeoutID | null)[];
/** @type {TimeoutID|null} */
declare var newTreeCuttingHandler: TimeoutID | null;
/** @deprecated Old Save Variable */
declare var treeCutLimit: number;
