(() => {
    const id = 'auto-eat';
    const title = 'AutoEat';
    const desc =
        "AutoEat in combat will only eat when your HP is below calculated max hit, to at least the max hit value, and if in combat to as close to maxhp as possible without wasting food. Also, if you don't have dungeon equipment swapping and you run out of food without AutoRun also enabled in a dungeon, you will just die.";
    const imgSrc = 'assets/media/shop/autoeat.svg';

    //@ts-expect-error
    const isInCombat = () => player.manager.isInCombat;

    const shouldBeEating = (toFull) => {
        const hp = SEMIUtils.currentHP(); // this number is already multiplied
        const hpmax = SEMIUtils.maxHP();
        const hpdeficit = hpmax - hp;
        const currentFood = SEMIUtils.currentFood();
        const hpfood = player.getFoodHealing(currentFood.item);
        const adjustedMaxHit = SEMIUtils.adjustedMaxHit();
        const maxHitEatingCase = hp <= adjustedMaxHit && isInCombat() && hp != hpmax;
        const continueEatingCase = toFull && hpdeficit >= hpfood;

        //@ts-expect-error
        const thievingMaxHit = game.thieving.isActive ? game.thieving.currentNPC.maxHit * numberMultiplier : 0;

        const generalEatingCase =
            (hpdeficit > hpfood || hp <= thievingMaxHit) && !SEMIUtils.isCurrentSkill('Hitpoints');
        const haveFoodEquipped = currentFood.quantity > 0;
        const eatingCase = (maxHitEatingCase || generalEatingCase || continueEatingCase) && haveFoodEquipped;

        const logItAll = () => {
            console.log(
                'hp',
                hp,
                'hpmax',
                hpmax,
                'hpdeficit',
                hpdeficit,
                'currentFood',
                currentFood,
                'hpfood',
                hpfood,
                'adjustedMaxHit',
                adjustedMaxHit,
                'maxHitEatingCase',
                maxHitEatingCase,
                'thievingMaxHit',
                thievingMaxHit,
                'generalEatingCase',
                generalEatingCase,
                'eatingCase',
                eatingCase
            );
        };

        // if (eatingCase) logItAll();

        return eatingCase;
    };

    const autoEat = () => {
        // @ts-ignore-line

        if (shouldBeEating(false)) {
            player.eatFood();
            if (!SEMIUtils.isCurrentSkill('Hitpoints')) return;

            //run a loop to eat multiple times instantaneously until your HP reaches the point where eating any more would be wasteful
            let i = 0;
            while (shouldBeEating(true)) {
                i++;
                if (i > 999) {
                    console.error(
                        'Infinite loop inside AutoEat! Please report this to TheAlpacalypse on discord ASAP'
                    );
                    break;
                }
                player.eatFood();
            }
        }
        if (SEMIUtils.currentFood().quantity >= 1) {
            return;
        }

        //@ts-expect-error
        const isInDungeon = player.manager.areaData.type === 'Dungeon';
        const characterHasDungeonEquipmentSwap = player.modifiers.dungeonEquipmentSwapping === 1;
        if (SEMIUtils.currentFood().quantity === 0 && isInDungeon && !characterHasDungeonEquipmentSwap) {
            return;
        }
        for (let i = 0; i < player.food.slots.length; i++) {
            if (player.food.slots[i].quantity > 0) {
                return player.selectFood(i);
            }
        }
    };

    const onEnable = () => {
        const hpmax = SEMIUtils.maxHP();
        const adjustedMaxHit = SEMIUtils.adjustedMaxHit();
        if (hpmax <= adjustedMaxHit) {
            SEMIUtils.customNotify(
                'assets/media/monsters/ghost.svg',
                "WARNING: You are engaged with an enemy that can one-hit-kill you. \n Its damage-reduction-adjusted max hit is at or above your max HP. \n This script can't save you now.",
                { duration: 10000 }
            );
        }
    };

    SEMI.add(id, {
        ms: 100,
        onLoop: autoEat,
        onEnable,
        pluginType: SEMI.PLUGIN_TYPE.AUTO_COMBAT,
        title,
        imgSrc,
        desc,
    });
})();
