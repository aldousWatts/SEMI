## Your Environment Details

Browser used (or Steam): _

Version of SEMI you are using: _

## Summary

(Summarize the bug encountered concisely. Name exactly which plugin is causing issues by disabling all others.)

## Steps to reproduce

(How one can reproduce the issue - this is very important to help us diagnose.)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Save file and screenshot

(Paste your save file using code blocks (`\``) as there might be a specific issue unrelated to SEMI at play.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug ~needs-investigation
/severity <high: Completely broken/medium: Plugin broken/low: minor error>